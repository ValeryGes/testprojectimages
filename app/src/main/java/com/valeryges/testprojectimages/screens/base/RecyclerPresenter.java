package com.valeryges.testprojectimages.screens.base;


import com.valeryges.testprojectimages.Contract;
import com.valeryges.testprojectimages.database.IImageDaoModule;
import com.valeryges.testprojectimages.events.UpdateImageEvent;
import com.valeryges.testprojectimages.file_saving.IImageSaveHelper;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.utils.Logger;
import com.valeryges.testprojectimages.utils.RxBus;
import com.valeryges.testprojectimages.utils.RxUtils;
import com.valeryges.testprojectimages.utils.Transformers;

import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

public abstract class RecyclerPresenter<T extends IBaseRecyclerView> extends CommonPresenter<T>
        implements IRecyclerPresenter<T> {

    // Actually there can't be any error but just in case
    protected static final Action1<Throwable> onImageUpdateError = Logger::printThrowable;
    protected final Action1<Throwable> onFirstLoadError = this::showError;
    protected final Action1<Throwable> onNewPortionLoadError = throwable -> {
        Logger.printThrowable(throwable);
        checkAndCallOrAddToDelayed(view -> view.showPaginationError(throwable.getMessage()));
    };
    protected final Action1<IImageModel> onDatabaseSuccess = image -> {
        showHideProgress(false);
        RxBus.instance().send(new UpdateImageEvent(image));
    };
    protected final Action1<Throwable> onDatabaseError = this::showError;
    protected final Action1<IImageModel> onImageUpdateSuccess =
            image -> checkAndCallOrAddToDelayed(view -> view.updateImage(image));
    protected volatile int limit = Contract.PAGINATION_LIMIT;
    protected volatile int offset = getStartOffset();
    private volatile int count = 100;
    protected final Action1<List<IImageModel>> onFirstLoadSuccess = this::onFirstLoaded;
    protected final Action1<List<IImageModel>> onNewPortionLoadSuccess = this::onNewPortionLoaded;
    private Subscription busSucscription;

    @Override
    public void onPresenterCreated() {
        super.onPresenterCreated();
        busSucscription = RxBus
                .instance()
                .filter(UpdateImageEvent.class)
                .map(UpdateImageEvent::getImage)
                .compose(Transformers.loadAsync())
                .subscribe(onImageUpdateSuccess, onImageUpdateError);
    }

    @Override
    public void onPresenterDestroyed() {
        super.onPresenterDestroyed();
        RxUtils.unsubscribeIfNotNull(busSucscription);
    }

    @Override
    public void reload() {
        RxUtils.unsubscribeIfNotNull(getCompositeSubscription());
        offset = getStartOffset();
        load(false, onFirstLoadSuccess, onFirstLoadError);
    }

    @Override
    public void loadNewPortion() {
        load(false, onNewPortionLoadSuccess, onNewPortionLoadError);
    }

    protected int getStartOffset() {
        return 1;
    }

    protected void onFirstLoaded(List<IImageModel> images) {
        showHideProgress(false);
        checkAndCallOrAddToDelayed(view -> view.addFirstPortion(images, count));
    }

    protected void onNewPortionLoaded(List<IImageModel> images) {
        checkAndCallOrAddToDelayed(view -> view.addNewPortion(images, count));
    }

    @Override
    public void save(IImageModel image) {
        showHideProgress(true);
        getCompositeSubscription().add(
                getImageSaveHelper()
                        .save(image)
                        .compose(getDaoModule().saveTransformer())
                        .compose(Transformers.loadAsync())
                        .subscribe(onDatabaseSuccess, onDatabaseError));
    }

    @Override
    public void remove(IImageModel image) {
        showHideProgress(true);
        getCompositeSubscription().add(
                getImageSaveHelper()
                        .remove(image)
                        .compose(getDaoModule().removeTransformer())
                        .compose(Transformers.loadAsync())
                        .subscribe(onDatabaseSuccess, onDatabaseError));
    }

    protected abstract void load(boolean withProgress,
                                 Action1<List<IImageModel>> onSuccess,
                                 Action1<Throwable> onError);

    protected abstract IImageSaveHelper getImageSaveHelper();

    protected abstract IImageDaoModule getDaoModule();

    private void showError(Throwable throwable) {
        showHideProgress(false);
        Logger.printThrowable(throwable);
        checkAndCallOrAddToDelayed(view -> view.showError(throwable.getMessage()));
    }
}
