package com.valeryges.testprojectimages.screens.main.favorites_screen;


import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.database.IImageDaoModule;
import com.valeryges.testprojectimages.file_saving.IImageSaveHelper;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.base.RecyclerPresenter;
import com.valeryges.testprojectimages.utils.Transformers;

import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;

public class FavoritesPresenter extends RecyclerPresenter<IFavoritesView>
        implements IFavoritesPresenter {

    @Inject
    IImageDaoModule daoModule;

    @Inject
    IImageSaveHelper imageSaveHelper;

    private boolean firstStart = true;

    public FavoritesPresenter() {
        super();
        TestProjectApp.getAppComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (firstStart) {
            reload();
            firstStart = false;
        }
    }

    @Override
    protected void load(boolean withProgress, Action1<List<IImageModel>> onSuccess, Action1<Throwable> onError) {
        getCompositeSubscription().add(
                daoModule
                        .getImagesLocal(offset, limit)
                        .doOnNext(images -> offset = offset + images.size())
                        .compose(Transformers.loadAsync())
                        .subscribe(onSuccess, onError));
    }

    @Override
    protected void onFirstLoaded(List<IImageModel> images) {
        showHideProgress(false);
        checkAndCallOrAddToDelayed(listView -> listView.addFirstPortion(images, Integer.MAX_VALUE));
    }

    @Override
    protected void onNewPortionLoaded(List<IImageModel> images) {
        checkAndCallOrAddToDelayed(listView -> listView.addNewPortion(images, Integer.MAX_VALUE));
    }

    @Override
    protected IImageSaveHelper getImageSaveHelper() {
        return imageSaveHelper;
    }

    protected IImageDaoModule getDaoModule() {
        return daoModule;
    }

    @Override
    protected int getStartOffset() {
        return 0;
    }
}
