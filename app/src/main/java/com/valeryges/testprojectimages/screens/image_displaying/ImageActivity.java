package com.valeryges.testprojectimages.screens.image_displaying;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.image_displaying.image_displayer.ImageDisplayingFragment;

public class ImageActivity extends AppCompatActivity {

    public static final String EXTRA_IMAGE = TestProjectApp.getInstance().getPackageName()
            + ImageActivity.class.getSimpleName() + "EXTRA_IMAGE";

    private Toolbar toolbar;

    public static void start(Context context, IImageModel image) {
        Intent starter = new Intent(context, ImageActivity.class);
        starter.putExtra(EXTRA_IMAGE, image);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar();
        Intent intent = getIntent();
        if (savedInstanceState == null) {
            if (intent != null) {
                IImageModel iImageModel = intent.getParcelableExtra(EXTRA_IMAGE);
                if (iImageModel != null) {
                    init(iImageModel);
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void init(IImageModel iImageModel) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container,
                        ImageDisplayingFragment.newInstance(iImageModel),
                        ImageDisplayingFragment.class.getSimpleName())
                .commit();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.image_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
