package com.valeryges.testprojectimages.screens.base;


import com.valeryges.testprojectimages.mvp.IPresenter;
import com.valeryges.testprojectimages.mvp.IView;

public interface IBaseView<TPresenter extends IPresenter> extends IView<TPresenter> {
    
    void showProgress();

    void hideProgress();

    void showError(String message);

    void showSuccess(String message);

    void showWarning(String message);

    void hideSnackBar();
}
