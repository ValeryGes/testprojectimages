package com.valeryges.testprojectimages.screens.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.List;


public class BaseViewPagerAdapter extends FragmentStatePagerAdapter {

    private final List<FragmentInfoContainer> fragmentInfoContainers;
    private Context context;
    private SparseArray<Fragment> fragments = new SparseArray<>();

    public BaseViewPagerAdapter(@NonNull Context context,
                                FragmentManager fm,
                                @NonNull List<FragmentInfoContainer> fragmentInfoContainers) {
        super(fm);
        this.context = context.getApplicationContext();
        this.fragmentInfoContainers = fragmentInfoContainers;
    }

    @Override
    public Fragment getItem(int position) {
        FragmentInfoContainer fragmentInfoContainer = fragmentInfoContainers.get(position);
        return Fragment.instantiate(context,
                fragmentInfoContainer.getFragmentClass().getName(),
                fragmentInfoContainer.getArgs());
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        fragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        fragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Nullable
    Fragment getFragment(int position) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentInfoContainers.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return fragmentInfoContainers.size();
    }

    public static FragmentInfoContainer newFragmentInfoContainer(@NonNull Class<? extends CommonFragment> fragmentClass,
                                                                 @NonNull String title,
                                                                 @NonNull Bundle args) {
        return new FragmentInfoContainer(fragmentClass, title, args);
    }

    public static FragmentInfoContainer newFragmentInfoContainer(@NonNull Class<? extends CommonFragment> fragmentClass,
                                                                 @NonNull String title) {
        return new FragmentInfoContainer(fragmentClass, title, new Bundle());
    }

    public static class FragmentInfoContainer {
        private Class<? extends CommonFragment> mFragmentClass;
        private String mTitle;
        private Bundle mArgs;

        private FragmentInfoContainer(@NonNull Class<? extends CommonFragment> fragmentClass,
                                      @NonNull String title,
                                      @NonNull Bundle args) {
            mFragmentClass = fragmentClass;
            mTitle = title;
            mArgs = args;
        }


        public FragmentInfoContainer setFragmentClass(Class<? extends CommonFragment> fragmentClass) {
            mFragmentClass = fragmentClass;
            return this;
        }

        public FragmentInfoContainer setTitle(String title) {
            mTitle = title;
            return this;
        }

        public FragmentInfoContainer setArgs(Bundle args) {
            mArgs = args;
            return this;
        }

        private Class<? extends CommonFragment> getFragmentClass() {
            return mFragmentClass;
        }

        private String getTitle() {
            return mTitle;
        }

        private Bundle getArgs() {
            return mArgs;
        }
    }
}
