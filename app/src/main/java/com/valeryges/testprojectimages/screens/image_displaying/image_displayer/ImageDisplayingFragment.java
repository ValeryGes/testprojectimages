package com.valeryges.testprojectimages.screens.image_displaying.image_displayer;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.base.CommonFragment;
import com.valeryges.testprojectimages.screens.base.LoaderContract;
import com.valeryges.testprojectimages.utils.ImageUtils;

import uk.co.senab.photoview.PhotoView;

public class ImageDisplayingFragment extends CommonFragment<IImageDisplayingPresenter>
        implements IImageDisplayingView,
        View.OnClickListener {

    private static final String EXTRA_IMAGE = "extra_image";


    private PhotoView photoView;
    private FloatingActionButton fabSave;
    private FloatingActionButton fabRemove;

    private IImageModel currentImage;

    public static ImageDisplayingFragment newInstance(IImageModel image) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_IMAGE, image);
        ImageDisplayingFragment fragment = new ImageDisplayingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        photoView = (PhotoView) view.findViewById(R.id.photoView);
        fabSave = (FloatingActionButton) view.findViewById(R.id.fabSave);
        fabRemove = (FloatingActionButton) view.findViewById(R.id.fabDelete);
        fabSave.setOnClickListener(this);
        fabRemove.setOnClickListener(this);
        if (savedInstanceState == null) {
            Bundle args = getArguments();
            if (args != null) {
                currentImage = args.getParcelable(EXTRA_IMAGE);
            }
        } else {
            currentImage = savedInstanceState.getParcelable(EXTRA_IMAGE);
        }
        displayView();
        invalidateFabs();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_IMAGE, currentImage);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.fabSave) {
            save();
        } else if (id == R.id.fabDelete) {
            delete();
        }
    }

    private void save() {
        checkAndCallOrAddToDelayed(presenter -> presenter.save(currentImage));
    }

    private void delete() {
        checkAndCallOrAddToDelayed(presenter -> presenter.remove(currentImage));
    }

    @Override
    public void updateImage(IImageModel imageModel) {
        currentImage = imageModel;
        invalidateFabs();
    }

    @Override
    protected void init() {
        initPresenter(LoaderContract.IMAGE_DISPLAYING, this, ImageDisplayingPresenter::new);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_image;
    }

    private void displayView() {
        if (currentImage != null) {
            ImageUtils.loadImageWithoutCrop(photoView,
                    currentImage.getActualLink());
        }
    }

    private void invalidateFabs() {
        if (currentImage != null) {
            boolean isInFavorite = currentImage.getLocalId() != null;
            if (isInFavorite) {
                fabRemove.show();
                fabSave.hide();
            } else {
                fabRemove.hide();
                fabSave.show();
            }
        }
    }
}
