package com.valeryges.testprojectimages.screens.main.google_search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.screens.base.BaseRecyclerFragment;
import com.valeryges.testprojectimages.screens.base.LoaderContract;
import com.valeryges.testprojectimages.utils.KeyboardUtils;
import com.valeryges.testprojectimages.utils.NetworkUtils;


public final class GoogleSearchFragment extends BaseRecyclerFragment<IGoogleSearchPresenter>
        implements IGoogleSearchView,
        View.OnClickListener {

    private EditText etSearch;
    private Button bSearch;

    public static GoogleSearchFragment newInstance() {
        Bundle args = new Bundle();
        GoogleSearchFragment fragment = new GoogleSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        bSearch = (Button) view.findViewById(R.id.bSearch);
        bSearch.setOnClickListener(this);
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                findImages();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bSearch) {
            findImages();
        }
    }

    private void findImages() {
        KeyboardUtils.hideKeyboard(this);
        final String query = etSearch.getText().toString();
        if (TextUtils.isEmpty(query)) {
            showError(getString(R.string.input_something));
        } else if (!NetworkUtils.isConnected()) {
            showError(getString(R.string.no_internet_connection));
        } else {
            checkAndCallOrAddToDelayed(presenter -> presenter.findImages(query));
        }
    }


    @Override
    protected void init() {
        initPresenter(LoaderContract.GOOGLE_SEARCH_LOADER, this, GoogleSearchPresenter::new);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_google_search;
    }
}
