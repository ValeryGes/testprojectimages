package com.valeryges.testprojectimages.screens.main.favorites_screen;


import android.os.Bundle;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.base.BaseRecyclerFragment;
import com.valeryges.testprojectimages.screens.base.LoaderContract;

public class FavoritesFragment extends BaseRecyclerFragment<IFavoritesPresenter>
        implements IFavoritesView {

    public static FavoritesFragment newInstance() {
        Bundle args = new Bundle();
        FavoritesFragment fragment = new FavoritesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void init() {
        initPresenter(LoaderContract.FAVORITES_LOADER, this, FavoritesPresenter::new);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_favorites;
    }

    @Override
    public void updateImage(IImageModel imageModel) {
        updateCurrentImages(imageModel);
        if (imageModel.getLocalId() == null) {
            if (adapter != null) {
                int position = adapter.getItemPosition(imageModel);
                if (position != -1) {
                    adapter.remove(imageModel);
                    adapter.notifyItemRemoved(position);
                }
            }
        } else {
            if (!isPaginationEnabled()) {
                requestNextPortion();
            }
        }
    }

    @Override
    protected void updateCurrentImages(IImageModel iImageModel) {
        if (iImageModel.getLocalId() == null) {
            if (currentImages != null && !currentImages.isEmpty()) {
                int position = currentImages.indexOf(iImageModel);
                if (position != -1) {
                    currentImages.remove(iImageModel);
                }
            }
        }
    }
}
