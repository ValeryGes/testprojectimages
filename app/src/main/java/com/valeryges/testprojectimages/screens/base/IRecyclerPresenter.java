package com.valeryges.testprojectimages.screens.base;


import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.mvp.IPresenter;

public interface IRecyclerPresenter<T extends IBaseRecyclerView> extends IPresenter<T> {

    void reload();

    void loadNewPortion();

    void save(IImageModel image);

    void remove(IImageModel image);
}
