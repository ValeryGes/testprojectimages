package com.valeryges.testprojectimages.screens.base;

import com.valeryges.testprojectimages.models.IImageModel;

public interface ImageAdapterListener {

    void onImageClicked(IImageModel image);

    void onSaveRemoveButtonClicked(IImageModel image);
}
