package com.valeryges.testprojectimages.screens.main.facebook_screen;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.screens.base.BaseRecyclerFragment;
import com.valeryges.testprojectimages.screens.base.LoaderContract;

public class FacebookFragment extends BaseRecyclerFragment<IFacebookPresenter>
        implements IFacebookView,
        View.OnClickListener {

    private static final String EXTRA_IS_LOGGEDIN = "extraLoggedIn";

    private Button bLogin;
    private Button bLoadPhotos;

    private boolean isLoggedIn = false;

    public static FacebookFragment newInstance() {
        Bundle args = new Bundle();
        FacebookFragment fragment = new FacebookFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bLogin = (Button) view.findViewById(R.id.bLogin);
        bLoadPhotos = (Button) view.findViewById(R.id.bLoad);
        bLogin.setOnClickListener(this);
        bLoadPhotos.setOnClickListener(this);
        if (savedInstanceState != null) {
            isLoggedIn = savedInstanceState.getBoolean(EXTRA_IS_LOGGEDIN);
            invalidateLoginResult(isLoggedIn);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(EXTRA_IS_LOGGEDIN, isLoggedIn);
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        checkAndCallOrAddToDelayed(presenter
                -> presenter.onActivityResult(requestCode, resultCode, data));
    }

    @Override
    public Fragment getFragmentForResult() {
        return this;
    }

    @Override
    public void invalidateLoginResult(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
        bLogin.setText(isLoggedIn ? getString(R.string.logout) : getString(R.string.login));
        bLoadPhotos.setVisibility(isLoggedIn ? View.VISIBLE : View.GONE);
        if (!isLoggedIn) {
            clearData();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.bLogin) {
            login();
        } else if (id == R.id.bLoad) {
            loadPhotos();
        }
    }

    private void login() {
        checkAndCallOrAddToDelayed(IFacebookPresenter::signIn);
    }

    private void loadPhotos() {
        checkAndCallOrAddToDelayed(IFacebookPresenter::loadPhotos);
    }

    @Override
    protected void init() {
        initPresenter(LoaderContract.FACEBOOK_LOADER, this, FacebookPresenter::new);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_facebook;
    }

}
