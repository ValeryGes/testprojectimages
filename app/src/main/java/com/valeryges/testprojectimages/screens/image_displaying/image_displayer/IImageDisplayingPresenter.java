package com.valeryges.testprojectimages.screens.image_displaying.image_displayer;


import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.mvp.IPresenter;

public interface IImageDisplayingPresenter extends IPresenter<IImageDisplayingView> {
    void save(IImageModel image);

    void remove(IImageModel image);
}
