package com.valeryges.testprojectimages.screens.main.google_search;

import com.valeryges.testprojectimages.screens.base.IRecyclerPresenter;


interface IGoogleSearchPresenter extends IRecyclerPresenter<IGoogleSearchView> {

    void findImages(String query);

}
