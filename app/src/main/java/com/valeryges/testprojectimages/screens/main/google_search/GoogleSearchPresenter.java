package com.valeryges.testprojectimages.screens.main.google_search;

import android.text.TextUtils;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.database.IImageDaoModule;
import com.valeryges.testprojectimages.file_saving.IImageSaveHelper;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.network.modules.IGoogleSearchModule;
import com.valeryges.testprojectimages.screens.base.RecyclerPresenter;
import com.valeryges.testprojectimages.utils.NetworkUtils;
import com.valeryges.testprojectimages.utils.RxUtils;
import com.valeryges.testprojectimages.utils.Transformers;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;


public final class GoogleSearchPresenter extends RecyclerPresenter<IGoogleSearchView>
        implements IGoogleSearchPresenter {

    private String query;

    @Inject
    TestProjectApp app;

    @Inject
    IGoogleSearchModule googleSearchModule;

    @Inject
    IImageDaoModule daoModule;

    @Inject
    IImageSaveHelper imageSaveHelper;

    public GoogleSearchPresenter() {
        super();
        TestProjectApp.getAppComponent().inject(this);
    }

    @Override
    public void findImages(String query) {
        RxUtils.unsubscribeIfNotNull(getCompositeSubscription());
        this.query = query;
        offset = getStartOffset();
        load(true, onFirstLoadSuccess, onFirstLoadError);
    }


    @Override
    protected void load(boolean withProgress,
                      Action1<List<IImageModel>> onSuccess,
                      Action1<Throwable> onError) {
        if (TextUtils.isEmpty(query)) {
            checkAndCallOrAddToDelayed(view -> view.showError(app.getString(R.string.input_something)));
        } else if (NetworkUtils.isConnected()) {
            if (withProgress) {
                showHideProgress(true);
            }
            getCompositeSubscription().add(
                    googleSearchModule
                            .findImages(query, offset, limit)
                            .doOnNext(images -> offset = offset + images.size())
                            .flatMap(Observable::from)
                            .compose(daoModule.fetchLocalImageTransformer())
                            .toList()
                            .compose(Transformers.loadAsync())
                            .subscribe(onSuccess, onError));
        } else {
            checkAndCallOrAddToDelayed(listView ->
                    listView.showError(app.getString(R.string.no_internet_connection)));
        }
    }

    @Override
    protected IImageSaveHelper getImageSaveHelper() {
        return imageSaveHelper;
    }

    @Override
    protected IImageDaoModule getDaoModule() {
        return daoModule;
    }
}
