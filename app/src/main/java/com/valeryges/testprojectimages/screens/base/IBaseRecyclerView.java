package com.valeryges.testprojectimages.screens.base;

import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.mvp.IPresenter;

import java.util.List;


public interface IBaseRecyclerView<TPresenter extends IPresenter> extends IBaseView<TPresenter> {

    void addFirstPortion(List<IImageModel> images, int count);

    void addNewPortion(List<IImageModel> images, int count);

    void showPaginationError(String message);

    void updateImage(IImageModel imageModel);
}
