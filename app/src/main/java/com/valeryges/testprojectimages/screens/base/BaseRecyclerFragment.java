package com.valeryges.testprojectimages.screens.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.valeryges.testprojectimages.Contract;
import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.image_displaying.ImageActivity;
import com.valeryges.testprojectimages.utils.NetworkUtils;
import com.valeryges.testprojectimages.utils.OrientationUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerFragment<T extends IRecyclerPresenter> extends CommonFragment<T>
        implements IBaseRecyclerView<T>,
        ImageAdapterListener {
    private static final String EXTRA_CURRENT_IMAGES = "extraCurrentImages";
    private static final String EXTRA_IS_REFRESHING = "extraIsRefreshing";
    private static final String EXTRA_PAGINATION_ENABLED = "extraPaginationEnabled";


    private SwipeRefreshLayout srLayout;
    private RecyclerView rvLoadedResults;
    private TextView tvPlaceholder;

    protected List<IImageModel> currentImages;
    protected ImageAdapter adapter;
    private GridLayoutManager layoutManager;
    private boolean paginationEnabled;

    private final RecyclerView.OnScrollListener paginationListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int position = layoutManager.findLastVisibleItemPosition();
            int updatePosition = recyclerView.getAdapter().getItemCount() - 1
                    - (Contract.PAGINATION_LIMIT / 2);
            if (position >= updatePosition) {
                requestNextPortion();
                recyclerView.removeOnScrollListener(this);
                paginationEnabled = false;
            }
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            currentImages = savedInstanceState.getParcelableArrayList(EXTRA_CURRENT_IMAGES);
            paginationEnabled = savedInstanceState.getBoolean(EXTRA_PAGINATION_ENABLED);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        srLayout = (SwipeRefreshLayout) view.findViewById(R.id.srLayout);
        rvLoadedResults = (RecyclerView) view.findViewById(R.id.rvLoadedResults);
        tvPlaceholder = (TextView) view.findViewById(R.id.tvPlaceHolder);
        boolean isRefreshing = false;
        if (savedInstanceState != null) {
            isRefreshing = savedInstanceState.getBoolean(EXTRA_IS_REFRESHING, false);
        }

        initRecyclerView();
        initSwipeToRefresh(isRefreshing);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (currentImages != null) {
            outState.putParcelableArrayList(EXTRA_CURRENT_IMAGES, new ArrayList<>(currentImages));
            outState.putBoolean(EXTRA_PAGINATION_ENABLED, paginationEnabled);
        }
        if (srLayout != null) {
            outState.putBoolean(EXTRA_IS_REFRESHING, srLayout.isRefreshing());
        }
    }


    @Override
    public void addFirstPortion(List<IImageModel> images, int count) {
        srLayout.setRefreshing(false);
        adapter.clear();
        if (images != null && !images.isEmpty()) {
            currentImages = images;
            adapter.addAll(images);
            if (!images.isEmpty() && currentImages.size() < count) {
                enablePagination();
            }
        }
        adapter.notifyDataSetChanged();
        invalidatePlaceHolder();
    }

    @Override
    public void addNewPortion(List<IImageModel> images, int count) {
        srLayout.setRefreshing(false);
        if (currentImages == null) {
            currentImages = new ArrayList<>();
        }
        currentImages.addAll(images);
        adapter.addAll(images);
        adapter.notifyItemRangeInserted(adapter.getItemCount() - images.size(), images.size());
        if (!images.isEmpty() && currentImages.size() < count) {
            enablePagination();
        }
        invalidatePlaceHolder();
    }

    @Override
    public void showError(String message) {
        srLayout.setRefreshing(false);
        super.showError(message);
    }

    @Override
    public void showPaginationError(String message) {
        enablePagination();
        showError(message);
    }

    private void initSwipeToRefresh(boolean isRefreshing) {
        srLayout.setRefreshing(isRefreshing);
        srLayout.setOnRefreshListener(this::requestReload);
    }

    private void requestReload() {
        IRecyclerPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.reload();
        } else {
            // in this case presenter can't be null but just in case
            srLayout.setRefreshing(false);
        }
    }

    protected void requestNextPortion() {
        if (NetworkUtils.isConnected()) {
            IRecyclerPresenter presenter = getPresenter();
            if (presenter != null) {
                presenter.loadNewPortion();
            } else {
                // in this case presenter can't be null but just in case
                enablePagination();
            }
        } else {
            showError(getString(R.string.no_internet_connection));
            enablePagination();
        }
    }

    private void initRecyclerView() {
        if (currentImages != null && !currentImages.isEmpty()) {
            adapter = new ImageAdapter(getContext(), this);
            adapter.addAll(currentImages);
        } else {
            adapter = new ImageAdapter(getContext(), this);
        }
        rvLoadedResults.setAdapter(adapter);
        int spanCount = OrientationUtils.isPortrait(this) ? 3 : 5;
        layoutManager = new GridLayoutManager(getContext(), spanCount,
                GridLayoutManager.VERTICAL, false);
        rvLoadedResults.setLayoutManager(layoutManager);
        if (paginationEnabled) {
            enablePagination();
        }
        invalidatePlaceHolder();
    }

    protected void enablePagination() {
        rvLoadedResults.addOnScrollListener(paginationListener);
        paginationEnabled = true;
    }

    private void invalidatePlaceHolder() {
        tvPlaceholder.setVisibility((currentImages != null && !currentImages.isEmpty())
                ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onImageClicked(IImageModel image) {
        ImageActivity.start(getContext(), image);
    }

    @Override
    public void onSaveRemoveButtonClicked(IImageModel image) {
        if (image.getLocalId() == null) {
            checkAndCallOrAddToDelayed(presenter -> presenter.save(image));
        } else {
            checkAndCallOrAddToDelayed(presenter -> presenter.remove(image));
        }

    }

    protected void clearData() {
        if (adapter != null) {
            adapter.clear();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void updateImage(IImageModel imageModel) {
        updateCurrentImages(imageModel);
        if (adapter != null) {
            int position = adapter.getItemPosition(imageModel);
            if (position != -1) {
                IImageModel imageInAdapter = adapter.getItem(position);
                imageInAdapter.setLocalId(imageModel.getLocalId());
                imageInAdapter.setLocalLink(imageModel.getLocalLink());
                adapter.notifyItemChanged(position, imageInAdapter.getLocalId() != null);
            }
        }
    }

    protected void updateCurrentImages(IImageModel iImageModel) {
        if (currentImages != null && currentImages.isEmpty()) {
            int position = currentImages.indexOf(iImageModel);
            if (position != -1) {
                IImageModel imageInList = currentImages.get(position);
                imageInList.setLocalId(iImageModel.getLocalId());
                imageInList.setLocalLink(iImageModel.getLocalLink());
            }
        }
    }

    protected boolean isPaginationEnabled() {
        return paginationEnabled;
    }
}
