package com.valeryges.testprojectimages.screens.base;


final class NoImplementationException extends ClassCastException {

    NoImplementationException() {
    }

    NoImplementationException(String message) {
        super(message);
    }

}
