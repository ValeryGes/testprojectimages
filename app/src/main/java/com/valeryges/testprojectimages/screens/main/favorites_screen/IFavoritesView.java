package com.valeryges.testprojectimages.screens.main.favorites_screen;


import com.valeryges.testprojectimages.screens.base.IBaseRecyclerView;

public interface IFavoritesView extends IBaseRecyclerView<IFavoritesPresenter> {
}
