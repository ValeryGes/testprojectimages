package com.valeryges.testprojectimages.screens.base;

import android.support.annotation.Nullable;

import com.valeryges.testprojectimages.funcs.Func1;
import com.valeryges.testprojectimages.funcs.Func3;
import com.valeryges.testprojectimages.mvp.BasePresenter;
import com.valeryges.testprojectimages.utils.RxUtils;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import rx.subscriptions.CompositeSubscription;


public abstract class CommonPresenter<T extends IBaseView> extends BasePresenter<T> {

    private CompositeSubscription compositeSubscription;
    private Queue<Func1<T>> delayedFuncs;
    private boolean isProgressShowing;

    @Override
    public void onResume() {
        super.onResume();
        if(isProgressShowing) {
            showHideProgress(true);
        }
        T view = getView();
        if (view != null && !delayedFuncs.isEmpty()) {
            while (delayedFuncs.peek() != null) {
                delayedFuncs.poll().call(view);
            }
        }
    }

    @Override
    public void onPresenterCreated() {
        delayedFuncs = new ConcurrentLinkedQueue<>();
        super.onPresenterCreated();
    }

    @Override
    public void onPresenterDestroyed() {
        RxUtils.unsubscribeIfNotNull(compositeSubscription);
        delayedFuncs.clear();
        super.onPresenterDestroyed();
    }

    protected void checkAndCall(Func1<T> func1) {
        T view = getView();
        if (view != null) {
            func1.call(view);
        }
    }

    protected void checkAndCallOrAddToDelayed(Func1<T> func1) {
        T view = getView();
        if (view != null) {
            func1.call(view);
        } else {
            delayedFuncs.add(func1);
        }
    }

    @Nullable
    protected <R> R checkAndCallWithResult(Func3<T, R> func1) {
        T view = getView();
        if (view != null) {
            return func1.call(view);
        } else {
            return null;
        }
    }

    protected CompositeSubscription getCompositeSubscription() {
        compositeSubscription = RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
        return compositeSubscription;
    }

    protected void showHideProgress(boolean needToShow) {
        isProgressShowing = needToShow;
        Func1<T> func = needToShow
                ? T::showProgress
                : T::hideProgress;
        checkAndCallOrAddToDelayed(func);
    }
}
