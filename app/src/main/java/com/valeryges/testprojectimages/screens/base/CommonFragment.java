package com.valeryges.testprojectimages.screens.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.funcs.Func1;
import com.valeryges.testprojectimages.funcs.Func3;
import com.valeryges.testprojectimages.mvp.BaseLoaderFragmentView;
import com.valeryges.testprojectimages.mvp.IPresenter;
import com.valeryges.testprojectimages.utils.SnackBarUtil;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public abstract class CommonFragment<T extends IPresenter> extends BaseLoaderFragmentView<T>
        implements IBaseView<T> {

    private Snackbar snackbar;

    private ProgressDialog progressDialog;

    private Queue<Func1<T>> delayedFuncs;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delayedFuncs = new ConcurrentLinkedQueue<>();
        init();
    }

    @Override
    public void onDetach() {
        delayedFuncs.clear();
        super.onDetach();
    }

    protected abstract void init();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        T presenter = getPresenter();
        if (presenter != null && !delayedFuncs.isEmpty()) {
            while (delayedFuncs.peek() != null) {
                delayedFuncs.poll().call(presenter);
            }
        }
    }

    @Override
    public void onDestroyView() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        super.onDestroyView();
    }

    @Override
    public void showError(String message) {
        View view = getView();
        snackbar = SnackBarUtil.showErrorLong(view, message);
    }

    @Override
    public void showSuccess(String message) {
        View view = getView();
        snackbar = SnackBarUtil.showSuccessLong(view, message);
    }

    @Override
    public void showWarning(String message) {
        View view = getView();
        snackbar = SnackBarUtil.showWarningLong(view, message);
    }

    @Override
    public void hideSnackBar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.show();
            progressDialog.setContentView(R.layout.animated_progress);
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    protected void checkAndCall(Func1<T> func1) {
        T presenter = getPresenter();
        if (presenter != null) {
            func1.call(presenter);
        }
    }

    protected void checkAndCallOrAddToDelayed(Func1<T> func1) {
        T presenter = getPresenter();
        if (presenter != null) {
            func1.call(presenter);
        } else {
            delayedFuncs.add(func1);
        }
    }

    @Nullable
    protected <R> R checkAndCallWithResult(Func3<T, R> func1) {
        T presenter = getPresenter();
        if (presenter != null) {
            return func1.call(presenter);
        } else {
            return null;
        }
    }

    @LayoutRes
    protected abstract int getLayoutId();

}
