package com.valeryges.testprojectimages.screens.base;


public final class LoaderContract {

    private static int loaderCount = 0;

    public static final int GOOGLE_SEARCH_LOADER = getLoaderId();
    public static final int FACEBOOK_LOADER = getLoaderId();
    public static final int FAVORITES_LOADER = getLoaderId();
    public static final int IMAGE_DISPLAYING = getLoaderId();

    private static int getLoaderId() {
        return loaderCount++;
    }

    private LoaderContract() {
        // no instance
    }
}
