package com.valeryges.testprojectimages.screens.main.facebook_screen;


import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.database.IImageDaoModule;
import com.valeryges.testprojectimages.facebook.FacebookHelper;
import com.valeryges.testprojectimages.facebook.IFacebookHelper;
import com.valeryges.testprojectimages.file_saving.IImageSaveHelper;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.base.IBaseView;
import com.valeryges.testprojectimages.screens.base.RecyclerPresenter;
import com.valeryges.testprojectimages.utils.Logger;
import com.valeryges.testprojectimages.utils.NetworkUtils;
import com.valeryges.testprojectimages.utils.RxUtils;
import com.valeryges.testprojectimages.utils.Transformers;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;

public class FacebookPresenter extends RecyclerPresenter<IFacebookView>
        implements IFacebookPresenter,
        FacebookHelper.FacebookAuthCallback {

    @Inject
    IFacebookHelper facebookHelper;

    @Inject
    TestProjectApp app;

    @Inject
    IImageDaoModule daoModule;

    @Inject
    IImageSaveHelper imageSaveHelper;

    private boolean isLoggedIn;
    private boolean firstStart = true;

    private volatile Queue<String> albumIds;
    private volatile String currentAlbumId;

    private final Action1<Pair<Boolean, Queue<String>>> onAlbumsLoaded = pair -> {
        final boolean needToShowProgress = pair.first;
        final Queue<String> albums = pair.second;
        albumIds = albums;
        if (albumIds != null && !albums.isEmpty()) {
            currentAlbumId = albumIds.poll();
            load(needToShowProgress, onFirstLoadSuccess, onFirstLoadError);
        } else {
            checkAndCallOrAddToDelayed(listView -> listView.addFirstPortion(new ArrayList<>(), 0));
            showHideProgress(false);
        }
    };

    public FacebookPresenter() {
        super();
        TestProjectApp.getAppComponent().inject(this);
        facebookHelper.attachHelper(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (firstStart) {
            isLoggedIn = facebookHelper.haveLoggedInFacebookUser();
            checkAndCallOrAddToDelayed(view -> view.invalidateLoginResult(isLoggedIn));
            firstStart = false;
        }
    }


    private void loadAlbums(final boolean withProgress) {
        if (!isLoggedIn) {
            checkAndCallOrAddToDelayed(listView -> listView.addFirstPortion(new ArrayList<>(), 0));
        } else if (NetworkUtils.isConnected()) {
            if (withProgress) {
                showHideProgress(true);
            }
            albumIds = null;
            getCompositeSubscription()
                    .add(facebookHelper
                            .loadAlbums()
                            .map(albums -> new Pair<>(withProgress, albums))
                            .compose(Transformers.loadAsync())
                            .subscribe(onAlbumsLoaded, onFirstLoadError));

        } else {
            checkAndCallOrAddToDelayed(listView ->
                    listView.showError(app.getString(R.string.no_internet_connection)));
        }
    }

    @Override
    protected void load(boolean withProgress, Action1<List<IImageModel>> onSuccess, Action1<Throwable> onError) {
        if (NetworkUtils.isConnected()) {
            if (withProgress) {
                showHideProgress(true);
            }
            if (currentAlbumId != null) {
                getCompositeSubscription().add(
                        facebookHelper
                                .loadPhotos(currentAlbumId, offset, limit)
                                .doOnNext(images -> {
                                    if (images.size() < limit) {
                                        currentAlbumId = albumIds.poll();
                                        offset = getStartOffset();
                                    } else {
                                        offset = offset + images.size();
                                    }
                                })
                                .flatMap(Observable::from)
                                .compose(daoModule.fetchLocalImageTransformer())
                                .toList()
                                .compose(Transformers.loadAsync())
                                .subscribe(onSuccess, onError));
            }
        } else {
            checkAndCallOrAddToDelayed(listView ->
                    listView.showError(app.getString(R.string.no_internet_connection)));
        }
    }

    @Override
    protected void onFirstLoaded(List<IImageModel> images) {
        showHideProgress(false);
        checkAndCallOrAddToDelayed(listView -> listView.addFirstPortion(images, Integer.MAX_VALUE));
    }

    @Override
    protected void onNewPortionLoaded(List<IImageModel> images) {
        checkAndCallOrAddToDelayed(listView -> listView.addNewPortion(images, Integer.MAX_VALUE));
    }

    @Override
    public void onSuccess(String token) {
        isLoggedIn = true;
        checkAndCallOrAddToDelayed(view -> view.invalidateLoginResult(isLoggedIn));
    }

    @Override
    public void onFail(@Nullable Throwable throwable) {
        Logger.printThrowable(throwable);
        checkAndCallOrAddToDelayed(view -> view.showError(app.getString(R.string.authorization_failed)));
    }

    @Override
    public void onCancel() {
        checkAndCallOrAddToDelayed(view -> view.showError(app.getString(R.string.authorization_canceled)));
    }

    @Override
    public Fragment getFragmentForResult() {
        return checkAndCallWithResult(IFacebookView::getFragmentForResult);
    }

    @Override
    public void onLogout() {
        isLoggedIn = false;
        currentAlbumId = null;
        albumIds = null;
        checkAndCallOrAddToDelayed(IBaseView::hideProgress);
        checkAndCallOrAddToDelayed(view -> view.invalidateLoginResult(isLoggedIn));
    }

    @Override
    public void loadPhotos() {
        RxUtils.unsubscribeIfNotNull(getCompositeSubscription());
        offset = getStartOffset();
        loadAlbums(true);
    }

    @Override
    public void reload() {
        RxUtils.unsubscribeIfNotNull(getCompositeSubscription());
        offset = getStartOffset();
        loadAlbums(false);
    }


    @Override
    protected int getStartOffset() {
        return 0;
    }

    @Override
    public void signIn() {
        if (facebookHelper.haveLoggedInFacebookUser()) {
            checkAndCallOrAddToDelayed(IBaseView::showProgress);
            facebookHelper.logOut();
        } else {
            facebookHelper.auth();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected IImageSaveHelper getImageSaveHelper() {
        return imageSaveHelper;
    }

    @Override
    protected IImageDaoModule getDaoModule() {
        return daoModule;
    }
}
