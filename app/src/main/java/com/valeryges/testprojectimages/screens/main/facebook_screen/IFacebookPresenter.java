package com.valeryges.testprojectimages.screens.main.facebook_screen;


import android.content.Intent;

import com.valeryges.testprojectimages.screens.base.IRecyclerPresenter;

public interface IFacebookPresenter extends IRecyclerPresenter<IFacebookView> {

    void loadPhotos();

    void signIn();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
