package com.valeryges.testprojectimages.screens.main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.screens.base.BaseViewPagerAdapter;
import com.valeryges.testprojectimages.screens.main.facebook_screen.FacebookFragment;
import com.valeryges.testprojectimages.screens.main.favorites_screen.FavoritesFragment;
import com.valeryges.testprojectimages.screens.main.google_search.GoogleSearchFragment;
import com.valeryges.testprojectimages.utils.KeyboardUtils;

import java.util.ArrayList;
import java.util.List;

public final class MainActivity extends AppCompatActivity {

    private BaseViewPagerAdapter vpAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        ViewPager vpScreens = (ViewPager) findViewById(R.id.vpScreens);
        initScreens(tabs, vpScreens);
    }

    private void initScreens(TabLayout tabs, ViewPager viewPager) {
        viewPager.setOffscreenPageLimit(2);
        List<BaseViewPagerAdapter.FragmentInfoContainer> fragmentInfoContainers = new ArrayList<>();
        fragmentInfoContainers.add(BaseViewPagerAdapter
                .newFragmentInfoContainer(GoogleSearchFragment.class,
                        getString(R.string.google_title)));
        fragmentInfoContainers.add(BaseViewPagerAdapter
                .newFragmentInfoContainer(FacebookFragment.class,
                        getString(R.string.facebook_title)));
        fragmentInfoContainers.add(BaseViewPagerAdapter
                .newFragmentInfoContainer(FavoritesFragment.class,
                        getString(R.string.favorites_title)));
        vpAdapter = new BaseViewPagerAdapter(this, getSupportFragmentManager(), fragmentInfoContainers);
        viewPager.setAdapter(vpAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                KeyboardUtils.hideKeyboard(MainActivity.this);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        KeyboardUtils.hideKeyboard(this);
        super.onBackPressed();
    }
}
