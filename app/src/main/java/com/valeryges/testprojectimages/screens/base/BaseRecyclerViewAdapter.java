package com.valeryges.testprojectimages.screens.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Base adapter for recycler view
 */
public abstract class BaseRecyclerViewAdapter<TData, TViewHolder extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<TViewHolder> {

    private final Context context;
    private final LayoutInflater inflater;
    protected final List<TData> data;

    public BaseRecyclerViewAdapter(@NonNull final Context context) {
        this.context = context.getApplicationContext();
        this.inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
    }

    public BaseRecyclerViewAdapter(@NonNull final Context context, @NonNull List<TData> data) {
        this.context = context.getApplicationContext();
        this.inflater = LayoutInflater.from(context);
        this.data = new ArrayList<>(data);
    }

    protected Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public TData getItem(final int position) throws ArrayIndexOutOfBoundsException {
        return data.get(position);
    }

    public boolean add(TData object) {
        return data.add(object);
    }

    public boolean remove(TData object) {
        return data.remove(object);
    }

    public TData remove(int position) {
        return data.remove(position);
    }

    public void clear() {
        data.clear();
    }

    public boolean addAll(@NonNull Collection<? extends TData> collection) {
        return data.addAll(collection);
    }

    public List<TData> getDataCopy() {
        return new ArrayList<>(data);
    }

    protected LayoutInflater getInflater() {
        return inflater;
    }

    public int getItemPosition(TData object) {
        return data.indexOf(object);
    }

    public void insert(TData object, int position) {
        data.add(position, object);
    }

    public void insertAll(Collection<? extends TData> object, int position) {
        data.addAll(position, object);
    }
}
