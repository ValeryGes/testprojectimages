package com.valeryges.testprojectimages.screens.main.favorites_screen;


import com.valeryges.testprojectimages.screens.base.IRecyclerPresenter;

public interface IFavoritesPresenter extends IRecyclerPresenter<IFavoritesView> {
}
