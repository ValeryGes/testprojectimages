package com.valeryges.testprojectimages.screens.base;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.utils.ImageUtils;
import com.valeryges.testprojectimages.widgets.SquareImageView;

import java.lang.ref.WeakReference;
import java.util.List;

public class ImageAdapter extends BaseRecyclerViewAdapter<IImageModel, ImageAdapter.ImageHolder>
        implements ImageAdapterListener {

    private WeakReference<ImageAdapterListener> imageAdapterListenerWeakReference;

    public ImageAdapter(@NonNull Context context, ImageAdapterListener imageAdapterListener) {
        super(context);
        imageAdapterListenerWeakReference = new WeakReference<>(imageAdapterListener);
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(getInflater().inflate(R.layout.item_image, parent, false), this);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        holder.bindView(getItem(position));
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position, List<Object> payloads) {
        if (payloads != null && !payloads.isEmpty()) {
            Object payload = payloads.get(0);
            if (payload.getClass().equals(Boolean.class)) {
                holder.postBind((Boolean) payload);
            }
        } else {
            super.onBindViewHolder(holder, position, payloads);
        }
    }

    @Override
    public void onImageClicked(IImageModel image) {
        ImageAdapterListener listener = imageAdapterListenerWeakReference.get();
        if (listener != null) {
            listener.onImageClicked(image);
        }
    }

    @Override
    public void onSaveRemoveButtonClicked(IImageModel image) {
        ImageAdapterListener listener = imageAdapterListenerWeakReference.get();
        if (listener != null) {
            listener.onSaveRemoveButtonClicked(image);
        }
    }

    static class ImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private SquareImageView ivImage;
        private TextView tvInFavorites;
        private Button bAddToFavorites;
        private View vSelect;

        private ImageAdapterListener listener;

        ImageHolder(View itemView, ImageAdapterListener listener) {
            super(itemView);
            ivImage = (SquareImageView) itemView.findViewById(R.id.ivImage);
            tvInFavorites = (TextView) itemView.findViewById(R.id.tvInFavorites);
            bAddToFavorites = (Button) itemView.findViewById(R.id.bAddToFavorites);
            vSelect = itemView.findViewById(R.id.vSelect);
            vSelect.setOnClickListener(this);
            bAddToFavorites.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            IImageModel image = (IImageModel) itemView.getTag();
            if (id == R.id.vSelect) {
                listener.onImageClicked(image);
            } else if (id == R.id.bAddToFavorites) {
                listener.onSaveRemoveButtonClicked(image);
            }
        }

        void bindView(IImageModel image) {
            itemView.setTag(image);
            ImageUtils.loadImage(ivImage, image.getActualLink(), R.drawable.ic_image_placeholder);
            postBind(image.getLocalId() != null);
        }

        void postBind(boolean isInFavorites) {
            Context context = itemView.getContext();
            tvInFavorites.setText(isInFavorites
                    ? context.getString(R.string.saved)
                    : context.getString(R.string.not_saved));
            bAddToFavorites.setTag(isInFavorites);
            bAddToFavorites.setText(isInFavorites
                    ? context.getString(R.string.remove)
                    : context.getString(R.string.save));
        }
    }

}
