package com.valeryges.testprojectimages.screens.image_displaying.image_displayer;


import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.database.IImageDaoModule;
import com.valeryges.testprojectimages.events.UpdateImageEvent;
import com.valeryges.testprojectimages.file_saving.IImageSaveHelper;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.base.CommonPresenter;
import com.valeryges.testprojectimages.utils.RxBus;
import com.valeryges.testprojectimages.utils.Transformers;

import javax.inject.Inject;

import rx.functions.Action1;

public class ImageDisplayingPresenter extends CommonPresenter<IImageDisplayingView>
        implements IImageDisplayingPresenter {

    @Inject
    IImageDaoModule daoModule;

    @Inject
    IImageSaveHelper imageSaveHelper;

    private final Action1<IImageModel> onDatabaseSuccess = image -> {
        showHideProgress(false);
        RxBus.instance().send(new UpdateImageEvent(image));
        checkAndCallOrAddToDelayed(view -> view.updateImage(image));
    };

    private final Action1<Throwable> onDatabaseError = throwable -> {
        showHideProgress(false);

    };

    public ImageDisplayingPresenter() {
        super();
        TestProjectApp.getAppComponent().inject(this);
    }

    @Override
    public void save(IImageModel image) {
        showHideProgress(true);
        getCompositeSubscription().add(
                imageSaveHelper
                        .save(image)
                        .compose(daoModule.saveTransformer())
                        .compose(Transformers.loadAsync())
                        .subscribe(onDatabaseSuccess, onDatabaseError));
    }

    @Override
    public void remove(IImageModel image) {
        showHideProgress(true);
        getCompositeSubscription().add(
                imageSaveHelper
                        .remove(image)
                        .compose(daoModule.removeTransformer())
                        .compose(Transformers.loadAsync())
                        .subscribe(onDatabaseSuccess, onDatabaseError));
    }

}
