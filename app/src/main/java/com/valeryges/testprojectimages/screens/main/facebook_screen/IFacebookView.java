package com.valeryges.testprojectimages.screens.main.facebook_screen;


import android.support.v4.app.Fragment;

import com.valeryges.testprojectimages.screens.base.IBaseRecyclerView;

public interface IFacebookView extends IBaseRecyclerView<IFacebookPresenter> {
    Fragment getFragmentForResult();
    void invalidateLoginResult(boolean isLoggedIn);
}
