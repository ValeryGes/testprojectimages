package com.valeryges.testprojectimages.screens.image_displaying.image_displayer;


import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.screens.base.IBaseView;

public interface IImageDisplayingView extends IBaseView<IImageDisplayingPresenter> {
    void updateImage(IImageModel iImageModel);
}
