package com.valeryges.testprojectimages.file_saving;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.utils.Logger;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import rx.Observable;

public class ImageSaveHelper implements IImageSaveHelper {

    private static final String PREFIX = "IMG_";
    private static final String EXTENSION = ".jpg";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMdd_HHmmssms", Locale.getDefault());

    private final Context context;
    private final File dir;

    public ImageSaveHelper(TestProjectApp app) {
        context = app.getApplicationContext();
        dir = context.getFilesDir();
    }

    @Override
    public Observable<IImageModel> save(final IImageModel image) {
        return Observable.just(image)
                .flatMap(imageModel -> {
                    try {
                        URL url = new URL(imageModel.getNetworkLink());
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        return Observable.just(BitmapFactory.decodeStream(new BufferedInputStream(input)));
                    } catch (IOException e) {
                        Logger.printThrowable(e);
                        return Observable.error(e);
                    }
                })
//                .flatMap(imageModel -> {
//                    try {
//                        return Observable.just(Glide.with(context)
//                                .load(imageModel.getNetworkLink())
//                                .asBitmap()
//                                .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
//                                .get());
//                    } catch (InterruptedException e) {
//                        Logger.printThrowable(e);
//                        return Observable.error(e);
//                    } catch (ExecutionException e) {
//                        Logger.printThrowable(e);
//                        return Observable.error(e);
//                    }
//                })
                .map(this::decodeToFile)
                .map(uri -> {
                    image.setLocalLink(uri.toString());
                    return image;
                });
    }



    @Override
    public Observable<IImageModel> remove(final IImageModel image) {
        return Observable.just(image)
                .map(imageModel -> removeFile(imageModel.getLocalLink()))
                .map(removed -> {
                    if (removed) {
                        image.setLocalLink(null);
                    }
                    return image;
                });
    }

    @Override
    public Observable.Transformer<IImageModel, IImageModel> saveTransformer() {
        return iImageModelObservable -> iImageModelObservable.flatMap(this::save);
    }

    @Override
    public Observable.Transformer<IImageModel, IImageModel> removeTransformer() {
        return iImageModelObservable -> iImageModelObservable.flatMap(this::remove);
    }

    private File createImageFile() {
        // Create an image file name
        String timeStamp = sdf.format(new Date());
        String imageFileName = PREFIX + timeStamp + "_";
        try {
            File file = new File(dir, imageFileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            return file;
        } catch (IOException e) {
            Logger.printThrowable(e);
        }
        return null;
    }

    @Nullable
    @WorkerThread
    private Uri decodeToFile(@NonNull Bitmap bitmap) {
        FileOutputStream fos = null;
        try {
            File fileWithDecodedImage = createImageFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] bitmapData = bos.toByteArray();

            fos = new FileOutputStream(fileWithDecodedImage);
            fos.write(bitmapData);
            fos.flush();
            return Uri.fromFile(fileWithDecodedImage);
        } catch (Exception e) {
            Logger.printThrowable(e);
            return null;
        } finally {
            bitmap.recycle();
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean removeFile(String link) {
        if (link == null) {
            return false;
        }
        File file = new File(link);
        return file.delete();
    }
}
