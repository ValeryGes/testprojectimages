package com.valeryges.testprojectimages.file_saving;


import com.valeryges.testprojectimages.models.IImageModel;

import rx.Observable;

public interface IImageSaveHelper {

    Observable<IImageModel> save(IImageModel image);

    Observable<IImageModel> remove(IImageModel image);

    Observable.Transformer<IImageModel, IImageModel> saveTransformer();

    Observable.Transformer<IImageModel, IImageModel> removeTransformer();
}
