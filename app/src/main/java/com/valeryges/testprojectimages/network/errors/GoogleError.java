package com.valeryges.testprojectimages.network.errors;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleError {

    @Expose
    @SerializedName("error")
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {

        @Expose
        @SerializedName("code")
        private int code;
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("errors")
        private List<ErrorsBean> errors;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<ErrorsBean> getErrors() {
            return errors;
        }

        public void setErrors(List<ErrorsBean> errors) {
            this.errors = errors;
        }

        public static class ErrorsBean {

            @Expose
            @SerializedName("domain")
            private String domain;
            @Expose
            @SerializedName("reason")
            private String reason;
            @Expose
            @SerializedName("message")
            private String message;
            @Expose
            @SerializedName("extendedHelp")
            private String extendedHelp;

            public String getDomain() {
                return domain;
            }

            public void setDomain(String domain) {
                this.domain = domain;
            }

            public String getReason() {
                return reason;
            }

            public void setReason(String reason) {
                this.reason = reason;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getExtendedHelp() {
                return extendedHelp;
            }

            public void setExtendedHelp(String extendedHelp) {
                this.extendedHelp = extendedHelp;
            }
        }
    }
}
