package com.valeryges.testprojectimages.network.converters;


import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.models.converters.IConverter;
import com.valeryges.testprojectimages.network.responces.GoogleSearchResponse;

public interface IGoogleImageConverter
        extends IConverter<GoogleSearchResponse.ItemsBean, IImageModel> {
}
