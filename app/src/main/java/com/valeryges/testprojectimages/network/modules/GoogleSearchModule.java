package com.valeryges.testprojectimages.network.modules;


import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.network.api.IGoogleSearchApi;
import com.valeryges.testprojectimages.network.converters.GoogleImageConverter;
import com.valeryges.testprojectimages.network.responces.GoogleSearchResponse;

import java.util.List;

import rx.Observable;

public class GoogleSearchModule extends BaseRxModule<IGoogleSearchApi>
        implements IGoogleSearchModule {

    private static final String FILE_TYPE = "jpg";
    private static final String SEARCH_TYPE = "image";
    private static final String IMG_SIZE = "large";

    private GoogleErrorParser googleErrorParser;
    private GoogleImageConverter googleImageConverter;

    public GoogleSearchModule(IGoogleSearchApi api, GoogleErrorParser googleErrorParser) {
        super(api);
        this.googleErrorParser = googleErrorParser;
        googleImageConverter = new GoogleImageConverter();
    }

    @Override
    public Observable<List<IImageModel>> findImages(String query, Integer offset, Integer limit) {
        return api.findImages(TestProjectApp.getInstance().getString(R.string.google_api_key),
                TestProjectApp.getInstance().getString(R.string.cx),
                query,
                FILE_TYPE,
                SEARCH_TYPE,
                IMG_SIZE,
                offset,
                limit)
                .onErrorResumeNext(googleErrorParser.rxParseError())
                .map(GoogleSearchResponse::getItems)
                .compose(googleImageConverter.listOUTtoIN());
    }
}
