package com.valeryges.testprojectimages.network.api;


import com.valeryges.testprojectimages.injections.modules.GoogleNetworkModule;
import com.valeryges.testprojectimages.network.responces.GoogleSearchResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IGoogleSearchApi {

    @GET("customsearch/" + GoogleNetworkModule.API_VERSION_V1)
    Observable<GoogleSearchResponse> findImages(@Query("key") String apikey,
                                                @Query("cx") String cx,
                                                @Query("q") String query,
                                                @Query("filetype") String fileType,
                                                @Query("searchType") String searchType,
                                                @Query("imgSize") String imgSize,
                                                @Query("start") Integer offset,
                                                @Query("num") Integer limit);
}
