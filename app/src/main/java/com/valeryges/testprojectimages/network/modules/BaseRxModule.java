package com.valeryges.testprojectimages.network.modules;

/**
 * Base RxModule for wrapping Network realization of api for some entity
 *
 * @param <T> Type of Network interface
 */
public class BaseRxModule<T> {

    T api;

    public BaseRxModule(T api) {
        this.api = api;
    }
}
