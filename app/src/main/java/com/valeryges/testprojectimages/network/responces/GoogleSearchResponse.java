package com.valeryges.testprojectimages.network.responces;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleSearchResponse {

    @Expose
    @SerializedName("items")
    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {

        @Expose
        @SerializedName("link")
        private String link;
        @Expose
        @SerializedName("image")
        private ImageBean image;

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public ImageBean getImage() {
            return image;
        }

        public void setImage(ImageBean image) {
            this.image = image;
        }

        public static class ImageBean {

            @Expose
            @SerializedName("contextLink")
            private String contextLink;
            @Expose
            @SerializedName("height")
            private int height;
            @Expose
            @SerializedName("width")
            private int width;
            @Expose
            @SerializedName("byteSize")
            private int byteSize;
            @Expose
            @SerializedName("thumbnailLink")
            private String thumbnailLink;
            @Expose
            @SerializedName("thumbnailHeight")
            private int thumbnailHeight;
            @Expose
            @SerializedName("thumbnailWidth")
            private int thumbnailWidth;

            public String getContextLink() {
                return contextLink;
            }

            public void setContextLink(String contextLink) {
                this.contextLink = contextLink;
            }

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public int getByteSize() {
                return byteSize;
            }

            public void setByteSize(int byteSize) {
                this.byteSize = byteSize;
            }

            public String getThumbnailLink() {
                return thumbnailLink;
            }

            public void setThumbnailLink(String thumbnailLink) {
                this.thumbnailLink = thumbnailLink;
            }

            public int getThumbnailHeight() {
                return thumbnailHeight;
            }

            public void setThumbnailHeight(int thumbnailHeight) {
                this.thumbnailHeight = thumbnailHeight;
            }

            public int getThumbnailWidth() {
                return thumbnailWidth;
            }

            public void setThumbnailWidth(int thumbnailWidth) {
                this.thumbnailWidth = thumbnailWidth;
            }
        }
    }
}
