package com.valeryges.testprojectimages.network.modules;


import com.valeryges.testprojectimages.models.IImageModel;

import java.util.List;

import rx.Observable;

public interface IGoogleSearchModule {

    Observable<List<IImageModel>> findImages(String query, Integer offset, Integer limit);
}
