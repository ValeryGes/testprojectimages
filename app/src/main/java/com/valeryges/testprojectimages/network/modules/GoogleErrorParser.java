package com.valeryges.testprojectimages.network.modules;


import com.google.gson.Gson;
import com.valeryges.testprojectimages.R;
import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.network.errors.GoogleError;
import com.valeryges.testprojectimages.network.exceptions.ApiException;
import com.valeryges.testprojectimages.utils.Logger;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Func1;


public final class GoogleErrorParser {

    private static final int SERVER_ERROR_CODE = 500;

    private static final String TAG = GoogleErrorParser.class.getSimpleName();


    private final Gson gson;

    public GoogleErrorParser(Gson gson) {
        this.gson = gson;
    }

    public static boolean isServerConnectionProblem(Throwable throwable) {
        return throwable instanceof SocketException || throwable instanceof SocketTimeoutException;
    }

    public static boolean isConnectionProblem(Throwable throwable) {
        return throwable instanceof UnknownHostException || throwable instanceof ConnectException;
    }

    public <T> Func1<Throwable, Observable<T>> rxParseError() {
        return throwable -> Observable.error(parseError(throwable));
    }

    public <T> Observable<T> parseErrorObservable(HttpException e) {
        return Observable.error(parseError(e));
    }

    private Throwable parseError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            // return this exception in case of error with 500 code
            if (httpException.code() == SERVER_ERROR_CODE) {
                Logger.printThrowable(httpException);
                return new ApiException(SERVER_ERROR_CODE,
                        TestProjectApp.getInstance().getString(R.string.server_error));
            }
            return parseErrorResponseBody(((HttpException) throwable).response());
        } else if (isConnectionProblem(throwable)) {
            return new ApiException(-1,
                    TestProjectApp.getInstance().getString(R.string.no_internet_connection));
        } else if (isServerConnectionProblem(throwable)) {
            return new ApiException(SERVER_ERROR_CODE,
                    TestProjectApp.getInstance().getString(R.string.server_error));
        } else {
            return throwable;
        }
    }

    private Exception parseErrorResponseBody(Response<?> response) {
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        try {
            inputStreamReader = new InputStreamReader(response.errorBody().byteStream());
            bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            String newLine;
            while ((newLine = bufferedReader.readLine()) != null) {
                sb.append(newLine);
            }

            // Try to parse ServerError.class
            GoogleError googleError;
            try {
                googleError = gson.fromJson(sb.toString(), GoogleError.class);
            } catch (Exception e) {
                Logger.e(TAG, "Couldn't parse error response to GoogleError.class: " + e.getMessage());
                return e;
            }
            return new ApiException(googleError.getError().getCode(), googleError.getError().getMessage());


        } catch (Exception e) {
            Logger.printThrowable(e);
            return e;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    Logger.printThrowable(e);
                }
            }
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    Logger.printThrowable(e);
                }
            }
        }
    }


}
