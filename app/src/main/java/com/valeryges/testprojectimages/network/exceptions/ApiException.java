package com.valeryges.testprojectimages.network.exceptions;


/**
 * Error from server.
 */
public class ApiException extends Exception {

    private final Integer statusCode;

    private final String message;


    public ApiException(Integer statusCode, String message) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

}


