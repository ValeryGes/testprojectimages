package com.valeryges.testprojectimages.network.converters;


import android.support.annotation.Nullable;

import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.models.ImageModel;
import com.valeryges.testprojectimages.models.converters.BaseConverter;
import com.valeryges.testprojectimages.network.responces.GoogleSearchResponse;

public class GoogleImageConverter
        extends BaseConverter<GoogleSearchResponse.ItemsBean, IImageModel>
        implements IGoogleImageConverter {

    @Override
    public IImageModel convertINtoOUT(GoogleSearchResponse.ItemsBean inObject) {
        if (inObject == null) {
            return null;
        }
        ImageModel imageModel = new ImageModel();
        imageModel.setNetworkLink(inObject.getLink());
        return imageModel;
    }

    @Override
    public IImageModel convertINtoOUT(GoogleSearchResponse.ItemsBean inObject, @Nullable Object payload) {
        throw new UnsupportedOperationException();
    }

    @Override
    public GoogleSearchResponse.ItemsBean convertOUTtoIN(IImageModel outObject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public GoogleSearchResponse.ItemsBean convertOUTtoIN(IImageModel outObject, @Nullable Object payload) {
        throw new UnsupportedOperationException();
    }
}
