package com.valeryges.testprojectimages;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.valeryges.testprojectimages.injections.components.AppComponent;
import com.valeryges.testprojectimages.injections.components.DaggerAppComponent;
import com.valeryges.testprojectimages.injections.modules.AppModule;


public class TestProjectApp extends Application {

    private static TestProjectApp instance;
    private AppComponent appComponent;

    private static synchronized void initInstance(TestProjectApp app) {
        instance = app;
    }

    public static AppComponent getAppComponent() {
        return getInstance().appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initInstance(this);
        initDagger(this);
    }

    public static TestProjectApp getInstance() {
        return instance;
    }

    private void initDagger(TestProjectApp app) {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(app))
                .build();
    }


}
