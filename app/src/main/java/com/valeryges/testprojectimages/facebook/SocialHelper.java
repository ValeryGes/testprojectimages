package com.valeryges.testprojectimages.facebook;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Interface for authorization with social Networks
 */
public interface SocialHelper {

    String TAG = SocialHelper.class.getSimpleName();

    /**
     * Starts authorization flow
     */
    void auth();

    /**
     * Delivers result from {@link Activity#onActivityResult(int, int, Intent)}
     * or {@link android.support.v4.app.Fragment#onActivityResult(int, int, Intent)}
     * to Authorization SDK realization
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     */
    void onActivityResult(int requestCode, int resultCode, Intent data);


    /**
     * Callback interface from helper
     */
    interface AuthorizationCallback {

        /**
         * Method called after successful authorization
         *
         * @param token    Authorization token from network
         */
        void onSuccess(String token);

        /**
         * Called after failed authorization
         *
         * @param throwable - The throwable from social authorization SDK
         */
        void onFail(@Nullable Throwable throwable);
    }

}
