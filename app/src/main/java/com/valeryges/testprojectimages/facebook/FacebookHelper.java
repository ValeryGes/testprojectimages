package com.valeryges.testprojectimages.facebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.valeryges.testprojectimages.facebook.converters.FacebookImageConverter;
import com.valeryges.testprojectimages.facebook.converters.IFacebookImageConverter;
import com.valeryges.testprojectimages.facebook.responses.FacebookAlbumsResponse;
import com.valeryges.testprojectimages.facebook.responses.FacebookImageResponse;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.network.exceptions.ApiException;
import com.valeryges.testprojectimages.utils.Logger;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import rx.Observable;


/**
 * Implementation of {@link SocialHelper} for authorization with Facebook
 */
public class FacebookHelper implements IFacebookHelper {

    private static final String URL_ENCODER_ENCODING = "UTF-8";

    private static final String FACEBOOK_LINK_PAGE_FORMAT = "https://facebook.com/%s";
    private static final String FACEBOOK_CURRENT_USER_URL_FORMAT = "/me?fields=%s";
    private static final String FACEBOOK_SEARCH_PAGE_URL_FORMAT = "search?q=%s&type=page";
    private static final String FACEBOOK_GET_PAGE_URL_FORMAT = "%s?fields=cover,picture.type(large)";

    private static final String FIELDS = "fields";
    private static final String ALBUMS = "albums";
    private static final String IMAGES = "images";
    private static final String ME = "/me/";
    private static final String ME_PERMISSIONS = ME + "permissions/";
    private static final String PHOTOS = "/%s/photos";
    private static final String OFFSET = "offset";
    private static final String LIMIT = "limit";

    private CallbackManager callbackManager;
    private Gson gson;
    private WeakReference<FacebookAuthCallback> callbackWR;
    private IFacebookImageConverter converter;

    public FacebookHelper(Gson gson) {
        this.gson = gson;
        callbackManager = CallbackManager.Factory.create();
        converter = new FacebookImageConverter();
    }

    @Override
    public void attachHelper(@NonNull FacebookAuthCallback callback) {
        callbackWR = new WeakReference<>(callback);
        initHelper(callback);
    }

    private void initHelper(@NonNull FacebookAuthCallback callback) {
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        login(loginResult.getAccessToken().getToken());
                    }

                    @Override
                    public void onCancel() {
                        Logger.d(TAG, "Facebook login cancel");
                        callback.onCancel();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Logger.e(TAG, "Facebook login error ", error);
                        callback.onFail(error);
                    }
                });
    }

    @Override
    public void auth() {
        FacebookAuthCallback callback = callbackWR.get();
        if (callback == null || callback.getFragmentForResult() == null) {
            return;
        }
        LoginManager.getInstance().logInWithReadPermissions(callback.getFragmentForResult(),
                createListPermissions(Permission.PHOTOS, Permission.PUBLIC_PROFILE, Permission.ABOUT_ME));
    }

    @Override
    public void logOut() {
        new GraphRequest(AccessToken.getCurrentAccessToken(),
                ME_PERMISSIONS,
                null,
                HttpMethod.DELETE,
                graphResponse -> {
                    LoginManager.getInstance().logOut();
                    FacebookAuthCallback callback = callbackWR.get();
                    if (callback != null) {
                        callback.onLogout();
                    }
                })
                .executeAsync();

    }

    @Override
    public boolean haveLoggedInFacebookUser() {
        return AccessToken.getCurrentAccessToken() != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void login(String token) {
        FacebookAuthCallback callback = callbackWR.get();
        if (callback != null) {
            callback.onSuccess(token);
        }
    }

    private Collection<String> createListPermissions(Permission... permissions) {
        List<String> facebookPermissions = null;
        if (permissions != null && permissions.length > 0) {
            facebookPermissions = new ArrayList<>();
            for (Permission permission : permissions) {
                facebookPermissions.add(permission.getName());
            }
        }
        return facebookPermissions;
    }

    @Override
    public Observable<Queue<String>> loadAlbums() {
        return Observable.fromCallable(() -> {
            Bundle parameters = new Bundle();
            parameters.putString(FIELDS, ALBUMS);
            return parameters;
        }).map(parameters -> new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                ME,
                parameters,
                HttpMethod.GET)
                .executeAndWait())
                .flatMap(response -> {
                    if (response.getError() != null) {
                        return Observable.error(
                                new ApiException(response.getError().getErrorCode(),
                                        response.getError().getErrorMessage()));
                    }
                    JSONObject joMain = response.getJSONObject();
                    FacebookAlbumsResponse facebookResponse =
                            gson.fromJson(joMain.toString(), FacebookAlbumsResponse.class);
                    return Observable.just(facebookResponse);
                })
                .map(facebookAlbumsResponse -> {
                    Queue<String> albumIds = new LinkedList<>();
                    FacebookAlbumsResponse.AlbumsBean albumsBean = facebookAlbumsResponse.getAlbums();
                    if (albumsBean != null && albumsBean.getData() != null) {
                        for (FacebookAlbumsResponse.AlbumsBean.AlbumsDataBean data : albumsBean.getData()) {
                            albumIds.add(data.getId());
                        }
                    }
                    return albumIds;
                });
    }

    @Override
    public Observable<List<IImageModel>> loadPhotos(final String albumId, final int offset, final int limit) {
        return Observable.fromCallable(() -> {
            Bundle parameters = new Bundle();
            parameters.putString(FIELDS, IMAGES);
            parameters.putString(OFFSET, String.valueOf(offset));
            parameters.putString(LIMIT, String.valueOf(limit));
            return parameters;
        }).map(parameters -> new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                String.format(PHOTOS, albumId),
                parameters,
                HttpMethod.GET).executeAndWait())
                .flatMap(response -> {
                    if (response.getError() != null) {
                        return Observable.error(new ApiException(response.getError().getErrorCode(), response.getError().getErrorMessage()));
                    }
                    JSONObject joMain = response.getJSONObject();
                    Logger.d("Facebook photos:", joMain.toString());
                    FacebookImageResponse facebookResponse = gson.fromJson(joMain.toString(), FacebookImageResponse.class);
                    return Observable.just(facebookResponse);
                })
                .map(FacebookImageResponse::getData)
                .compose(converter.listOUTtoIN());

}

    /**
     * Callback interface from {@link FacebookHelper}
     */
    public interface FacebookAuthCallback extends SocialHelper.AuthorizationCallback {

        /**
         * Calls if user canceled authorization
         */
        void onCancel();

        /**
         * Return Instance of Fragment for receiving result from
         * FacebookActivity.
         *
         * @return Instance of {@link Fragment} for starting activity
         */
        Fragment getFragmentForResult();

        void onLogout();
    }

    @SuppressWarnings("WeakerAccess")
    public enum Permission {
        PUBLIC_PROFILE("public_profile"),
        ABOUT_ME("user_about_me"),
        PHOTOS("user_photos");

        private String permission;

        Permission(String permission) {
            this.permission = permission;
        }

        private String getName() {
            return permission;
        }
    }
}
