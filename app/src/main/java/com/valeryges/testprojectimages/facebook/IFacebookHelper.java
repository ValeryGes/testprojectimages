package com.valeryges.testprojectimages.facebook;


import android.support.annotation.NonNull;

import com.valeryges.testprojectimages.models.IImageModel;

import java.util.List;
import java.util.Queue;

import rx.Observable;

public interface IFacebookHelper extends SocialHelper {

    void attachHelper(@NonNull FacebookHelper.FacebookAuthCallback callback);

    void logOut();

    /**
     * Is user logged in his facebook account.
     */
    boolean haveLoggedInFacebookUser();

    Observable<Queue<String>> loadAlbums();

    Observable<List<IImageModel>> loadPhotos(final String albumId, final int offset, final int limit);
}

