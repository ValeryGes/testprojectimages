package com.valeryges.testprojectimages.facebook.converters;


import com.valeryges.testprojectimages.facebook.responses.FacebookImageResponse;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.models.converters.IConverter;

public interface IFacebookImageConverter
        extends IConverter<FacebookImageResponse.DataBean, IImageModel> {
}
