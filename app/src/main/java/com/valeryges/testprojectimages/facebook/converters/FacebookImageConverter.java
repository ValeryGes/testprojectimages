package com.valeryges.testprojectimages.facebook.converters;


import android.support.annotation.Nullable;

import com.valeryges.testprojectimages.facebook.responses.FacebookImageResponse;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.models.ImageModel;
import com.valeryges.testprojectimages.models.converters.BaseConverter;

import java.util.List;

public class FacebookImageConverter extends BaseConverter<FacebookImageResponse.DataBean, IImageModel>
        implements IFacebookImageConverter {
    @Override
    public IImageModel convertINtoOUT(FacebookImageResponse.DataBean inObject) {
        if (inObject == null) {
            return null;
        }

        List<FacebookImageResponse.DataBean.ImagesBean> imageBeens = inObject.getImages();
        if (imageBeens != null && !imageBeens.isEmpty()) {
            ImageModel image = new ImageModel();
            image.setNetworkLink(imageBeens.get(0).getSource());
            return image;
        } else {
            return null;
        }
    }

    @Override
    public IImageModel convertINtoOUT(FacebookImageResponse.DataBean inObject, @Nullable Object payload) {
        throw new UnsupportedOperationException();
    }

    @Override
    public FacebookImageResponse.DataBean convertOUTtoIN(IImageModel outObject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public FacebookImageResponse.DataBean convertOUTtoIN(IImageModel outObject, @Nullable Object payload) {
        throw new UnsupportedOperationException();
    }
}
