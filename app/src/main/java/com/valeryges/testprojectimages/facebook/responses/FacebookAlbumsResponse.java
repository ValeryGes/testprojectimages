package com.valeryges.testprojectimages.facebook.responses;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FacebookAlbumsResponse {

    @Expose
    @SerializedName("albums")
    private AlbumsBean albums;
    @Expose
    @SerializedName("id")
    private String id;

    public AlbumsBean getAlbums() {
        return albums;
    }

    public void setAlbums(AlbumsBean albums) {
        this.albums = albums;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class AlbumsBean {

        @Expose
        @SerializedName("data")
        private List<AlbumsDataBean> data;


        public List<AlbumsDataBean> getData() {
            return data;
        }

        public void setData(List<AlbumsDataBean> data) {
            this.data = data;
        }


        public static class AlbumsDataBean {

            @Expose
            @SerializedName("name")
            private String name;
            @Expose
            @SerializedName("id")
            private String id;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }
}
