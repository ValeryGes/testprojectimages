package com.valeryges.testprojectimages.events;


import com.valeryges.testprojectimages.models.IImageModel;

/**
 * Event for {@link com.valeryges.testprojectimages.utils.RxBus}
 * for image updating
 */
public class UpdateImageEvent {

    private final IImageModel image;

    public UpdateImageEvent(IImageModel image) {
        this.image = image;
    }

    public IImageModel getImage() {
        return image;
    }
}
