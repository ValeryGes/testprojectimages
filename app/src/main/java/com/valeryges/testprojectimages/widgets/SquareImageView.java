package com.valeryges.testprojectimages.widgets;


import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.valeryges.testprojectimages.R;

import static com.valeryges.testprojectimages.widgets.SquareImageView.Measure.BY_HEIGHT;
import static com.valeryges.testprojectimages.widgets.SquareImageView.Measure.BY_WIDTH;


/**
 * View extended from {@link ImageView} witch sets one of it's measure
 * (width or height) equals to another. By default measurement will be made by width
 */
public class SquareImageView extends ImageView {

    @Measure
    private int mMeasureBy;

    public SquareImageView(Context context) {
        super(context);
        init(context, null);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SquareImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mMeasureBy == BY_WIDTH) {
            int measuredWidth = getMeasuredWidth();
            //noinspection SuspiciousNameCombination
            setMeasuredDimension(measuredWidth, measuredWidth);
        } else {
            int measuredHeight = getMeasuredHeight();
            //noinspection SuspiciousNameCombination
            setMeasuredDimension(measuredHeight, measuredHeight);
        }
    }

    /**
     * Returns id of selected measure type
     *
     * @return one of {@link Measure}
     */
    public int getMeasureBy() {
        return mMeasureBy;
    }

    /**
     * Set attribute for measure
     *
     * @param measureBy one of {@link Measure}
     */
    public void setMeasureBy(int measureBy) {
        this.mMeasureBy = measureBy;
        invalidate();
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SquareImageView);
        @Measure int measure = typedArray.getInteger(R.styleable.SquareImageView_measured_by, BY_WIDTH);
        mMeasureBy = measure;
        typedArray.recycle();
    }

    /**
     * Interface with constants for Measure determination in {@link SquareImageView}
     */
    @IntDef({BY_WIDTH, BY_HEIGHT})
    public @interface Measure {
        /**
         * Constant for measurement by width
         */
        int BY_WIDTH = 0;
        /**
         * Constant for measurement by height
         */
        int BY_HEIGHT = 1;
    }

}
