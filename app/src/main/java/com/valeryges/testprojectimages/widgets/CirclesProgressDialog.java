package com.valeryges.testprojectimages.widgets;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.valeryges.testprojectimages.R;


/**
 * Just simple realization of custom progress bar
 */
public class CirclesProgressDialog extends FrameLayout {

    private TextView txtMessage;

    public CirclesProgressDialog(Context context) {
        this(context, null);
    }

    public CirclesProgressDialog(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CirclesProgressDialog(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CirclesProgressDialog(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init(getContext());
    }

    private void init(Context context) {
        if (context == null) {
            return;
        }

        Resources.Theme theme = context.getTheme();
        if (theme == null) {
            return;
        }

        txtMessage = (TextView) findViewById(R.id.txtMessage);
        View primaryCircle = findViewById(R.id.vPrimary);
        View accentCircle = findViewById(R.id.vAccent);
        View greenCircle = findViewById(R.id.vGreen);
        View violetCircle = findViewById(R.id.vViolet);

        AnimatorSet primarySet = createScale(primaryCircle);
        AnimatorSet accentSet = createScale(accentCircle);
        AnimatorSet greenSet = createScale(greenCircle);
        AnimatorSet violetSet = createScale(violetCircle);

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(primarySet);
        animatorSet.play(accentSet).after(primarySet);
        animatorSet.play(greenSet).after(accentSet);
        animatorSet.play(violetSet).after(greenSet);

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                // no implementation
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animatorSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // no implementation
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // no implementation
            }
        });
        animatorSet.start();
    }

    private AnimatorSet createScale(View circle) {
        ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(circle, "scaleX", 1.0f, 1.5f);
        ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(circle, "scaleY", 1.0f, 1.5f);
        scaleUpX.setDuration(100);
        scaleUpY.setDuration(100);

        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(circle, "scaleX", 1.5f, 1.0f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(circle, "scaleY", 1.5f, 1.0f);
        scaleDownX.setDuration(100);
        scaleDownY.setDuration(100);

        AnimatorSet upSet = new AnimatorSet();
        upSet.play(scaleUpX).with(scaleUpY);
        AnimatorSet downSet = new AnimatorSet();
        downSet.play(scaleDownX).with(scaleDownY);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(upSet).before(downSet);

        return animatorSet;
    }

    public String getText() {
        return txtMessage.getText().toString();
    }

    public void setText(int message) {
        txtMessage.setText(message);
    }

    public void setText(String message) {
        txtMessage.setText(message);
    }
}
