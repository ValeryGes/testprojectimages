package com.valeryges.testprojectimages.injections.modules;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.valeryges.testprojectimages.BuildConfig;
import com.valeryges.testprojectimages.network.api.IGoogleSearchApi;
import com.valeryges.testprojectimages.network.modules.GoogleErrorParser;
import com.valeryges.testprojectimages.network.modules.GoogleSearchModule;
import com.valeryges.testprojectimages.network.modules.IGoogleSearchModule;

import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GoogleNetworkModule {

    public static final String API_VERSION_V1 = "v1";


    private static final String BASE_URL = BuildConfig.ENDPOINT;

    @Singleton
    @Provides
    Gson proivideGson() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setLenient()
                .create();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(Gson gson) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level logLevel = BuildConfig.DEBUG ?
                HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
        httpLoggingInterceptor.setLevel(logLevel);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder();

                    Request request = requestBuilder
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                })
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }

    @Singleton
    @Provides
    GoogleErrorParser provideGoogleErrorParser(Gson gson) {
        return new GoogleErrorParser(gson);
    }

    @Singleton
    @Provides
    IGoogleSearchModule provideGoogleSearchModule(Retrofit retrofit, GoogleErrorParser googleErrorParser) {
        return new GoogleSearchModule(retrofit.create(IGoogleSearchApi.class), googleErrorParser);
    }


}
