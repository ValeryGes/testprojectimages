package com.valeryges.testprojectimages.injections.modules;

import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.database.IImageDaoModule;
import com.valeryges.testprojectimages.database.ImageDaoModule;
import com.valeryges.testprojectimages.file_saving.IImageSaveHelper;
import com.valeryges.testprojectimages.file_saving.ImageSaveHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {


    @Singleton
    @Provides
    IImageDaoModule provideImageDaoModule(TestProjectApp testProjectApp) {
        return new ImageDaoModule(testProjectApp);
    }

    @Singleton
    @Provides
    IImageSaveHelper provideImageSaveHelper(TestProjectApp testProjectApp) {
        return new ImageSaveHelper(testProjectApp);
    }
}
