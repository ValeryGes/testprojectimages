package com.valeryges.testprojectimages.injections.modules;

import com.google.gson.Gson;
import com.valeryges.testprojectimages.facebook.FacebookHelper;
import com.valeryges.testprojectimages.facebook.IFacebookHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class FacebookModule {

    @Provides
    IFacebookHelper provideFacebookHelper(Gson gson) {
        return new FacebookHelper(gson);
    }
}
