package com.valeryges.testprojectimages.injections.modules;

import com.valeryges.testprojectimages.TestProjectApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private TestProjectApp application;

    public AppModule(TestProjectApp application) {
        this.application = application;
    }

    @Singleton
    @Provides
    TestProjectApp provideApplication() {
        return application;
    }

}
