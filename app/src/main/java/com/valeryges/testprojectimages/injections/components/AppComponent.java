package com.valeryges.testprojectimages.injections.components;


import com.valeryges.testprojectimages.injections.modules.AppModule;
import com.valeryges.testprojectimages.injections.modules.DatabaseModule;
import com.valeryges.testprojectimages.injections.modules.FacebookModule;
import com.valeryges.testprojectimages.injections.modules.GoogleNetworkModule;
import com.valeryges.testprojectimages.screens.image_displaying.image_displayer.ImageDisplayingPresenter;
import com.valeryges.testprojectimages.screens.main.facebook_screen.FacebookPresenter;
import com.valeryges.testprojectimages.screens.main.favorites_screen.FavoritesPresenter;
import com.valeryges.testprojectimages.screens.main.google_search.GoogleSearchPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class, GoogleNetworkModule.class,
        FacebookModule.class, DatabaseModule.class})
public interface AppComponent {

    void inject(GoogleSearchPresenter googleSearchPresenter);

    void inject(FacebookPresenter facebookPresenter);

    void inject(FavoritesPresenter favoritesPresenter);

    void inject(ImageDisplayingPresenter imageDisplayingPresenter);
}
