package com.valeryges.testprojectimages.database;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class ImageContentProvider extends ContentProvider {

    private static final String WHERE_ID_EQUALS = ImageDatabaseHelper.KEY_ID + " =?";
    private static final int IMAGES = 1;
    private static final UriMatcher URI_MATCHER;

    private static final String UNKNOWN_URI = "Unknown URI ";

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(ContentConstants.AUTHORITY, ContentConstants.IMAGE_PATH, IMAGES);
    }

    ImageDatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new ImageDatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor cursor;
        String table;
        String[] columns;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case IMAGES:
                table = ImageDatabaseHelper.TABLE_IMAGES;
                columns = new String[]{ImageDatabaseHelper.KEY_ID,
                        ImageDatabaseHelper.KEY_NETWORK_LINK,
                        ImageDatabaseHelper.KEY_LOCAL_LINK};
                final String finalSelection;
                final String[] finalSelectionArgs;
                if (!TextUtils.isEmpty(selection)) {
                    if (TextUtils.isDigitsOnly(selection)) {
                        // selection contains id
                        finalSelection = ImageDatabaseHelper.TABLE_IMAGES
                                + "." + ImageDatabaseHelper.KEY_ID
                                + " =?";

                    } else {
                        // selection contains network link
                        finalSelection = ImageDatabaseHelper.TABLE_IMAGES
                                + "." + ImageDatabaseHelper.KEY_NETWORK_LINK
                                + " =?";
                    }
                    finalSelectionArgs = new String[]{selection};
                } else {
                    // selection doesn't contains anything
                    finalSelection = null;
                    finalSelectionArgs = null;
                }
                final String limit;
                if (selectionArgs != null && selectionArgs.length >= 2) {
                    // selectionArgs contains limit and offset
                    limit = selectionArgs[0] +"," + selectionArgs[1];
                } else {
                    limit = null;
                }
                final String finalSort;
                if (sortOrder == null) {
                    finalSort = ImageDatabaseHelper.KEY_ID + " ASC";
                } else {
                    finalSort = null;
                }

                cursor = db.query(table, columns, finalSelection, finalSelectionArgs, null, null, finalSort, limit);
                break;

            default:
                throw new IllegalArgumentException(UNKNOWN_URI + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }


    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Uri resultUri;
        long id;
        switch (URI_MATCHER.match(uri)) {
            case IMAGES:
                id = db.insert(ImageDatabaseHelper.TABLE_IMAGES, null, values);
                break;
            default:
                throw new IllegalArgumentException(UNKNOWN_URI + uri);
        }
        resultUri = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(resultUri, null);

        return resultUri;

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int count;
        switch (URI_MATCHER.match(uri)) {
            case IMAGES:
                count = db.delete(ImageDatabaseHelper.TABLE_IMAGES, WHERE_ID_EQUALS, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException(UNKNOWN_URI + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int count;
        switch (URI_MATCHER.match(uri)) {
            case IMAGES:
                count = db.update(ImageDatabaseHelper.TABLE_IMAGES, values, WHERE_ID_EQUALS, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException(UNKNOWN_URI + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }


    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }
}
