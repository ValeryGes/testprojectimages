package com.valeryges.testprojectimages.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.valeryges.testprojectimages.BuildConfig;

public class ImageDatabaseHelper extends SQLiteOpenHelper {

    // base database info
    private static final int DATABASE_VERSION = BuildConfig.database_version;
    private static final String DATABASE_NAME = BuildConfig.database_name;

    // Table names
    static final String TABLE_IMAGES = "images";

    // key fields
    static final String KEY_ID = "_id";
    static final String KEY_NETWORK_LINK = "image_network_link";
    static final String KEY_LOCAL_LINK = "image_local_link";

    // query constants
    private static final String CREATE_TABLE = "create table";
    private static final String INTEGER = "integer";
    private static final String PRIMARY_KEY = "primary key";
    private static final String AUTOINCREMENT = "autoincrement";
    private static final String TEXT = "text";
    private static final String NOT_NULL = "not null";
    private static final String TEXT_NOT_NULL = TEXT + " " + NOT_NULL;

    private static final String IMAGES_CREATE_QUERY =
            CREATE_TABLE + " " + TABLE_IMAGES + " ("
                    + KEY_ID + " " + INTEGER + " " + PRIMARY_KEY + " " + AUTOINCREMENT + ", "
                    + KEY_NETWORK_LINK + " " + TEXT_NOT_NULL + ", "
                    + KEY_LOCAL_LINK + " " + TEXT + ");";


    public ImageDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(IMAGES_CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
