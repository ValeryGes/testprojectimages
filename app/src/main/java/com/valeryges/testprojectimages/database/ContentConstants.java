package com.valeryges.testprojectimages.database;


import android.net.Uri;


public final class ContentConstants {
    private ContentConstants() {
    }

    static final String SCHEME = "content://";
    static final String AUTHORITY = "com.valeryges.ImageContentProvider";
    static final String IMAGE_PATH = "/images/";


    public static final Uri IMAGE_URI = Uri.parse(SCHEME + AUTHORITY + IMAGE_PATH);

}
