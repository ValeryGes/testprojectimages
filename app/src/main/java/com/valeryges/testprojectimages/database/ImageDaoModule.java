package com.valeryges.testprojectimages.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.valeryges.testprojectimages.TestProjectApp;
import com.valeryges.testprojectimages.models.IImageModel;
import com.valeryges.testprojectimages.models.ImageModel;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class ImageDaoModule implements IImageDaoModule {

    private Context context;

    public ImageDaoModule(TestProjectApp app) {
        context = app;
    }

    @Override
    public Observable<List<IImageModel>> getImagesLocal(int offset, int limit) {
        return Observable.just(context)
                .map(context1 -> {
                    List<IImageModel> images = new ArrayList<>();
                    String[] selectionArgs = {String.valueOf(offset), String.valueOf(limit)};
                    Cursor cursor = null;
                    try {
                        cursor = context1
                                .getContentResolver()
                                .query(ContentConstants.IMAGE_URI, null, null, selectionArgs, null);
                        images = parseCursorToList(cursor);
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                    return images;
                });
    }

    @Override
    public Observable<IImageModel> getById(final Long id) {
        return Observable.just(context)
                .map(context1 -> {
                    IImageModel image = null;
                    Cursor cursor = null;
                    try {
                        cursor = context1
                                .getContentResolver()
                                .query(ContentConstants.IMAGE_URI, null, String.valueOf(id), null, null);
                        image = parseCursorToSingle(cursor);
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                    return image;
                });
    }

    @Override
    public Observable<IImageModel> getByNetworkLink(final String networkLink) {
        return Observable.just(context)
                .map(context1 -> {
                    IImageModel image = null;
                    Cursor cursor = null;
                    try {
                        cursor = context1
                                .getContentResolver()
                                .query(ContentConstants.IMAGE_URI, null, networkLink, null, null);
                        image = parseCursorToSingle(cursor);
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                    return image;
                });
    }

    @Override
    public Observable<IImageModel> remove(final IImageModel iImageModel) {
        return Observable.just(context)
                .map(context1 -> context1
                        .getContentResolver()
                        .delete(ContentConstants.IMAGE_URI, null,
                                new String[]{String.valueOf(iImageModel.getLocalId())}))
                .map(ignored -> iImageModel)
                .doOnNext(image -> image.setLocalId(null));

    }

    @Override
    public Observable<IImageModel> save(final IImageModel iImageModel) {
        return Observable.just(context)
                .map(context1 -> {
                    ContentValues values = new ContentValues();
                    values.put(ImageDatabaseHelper.KEY_NETWORK_LINK, iImageModel.getNetworkLink());
                    values.put(ImageDatabaseHelper.KEY_LOCAL_LINK, iImageModel.getLocalLink());

                    Uri uri = context1.getContentResolver().insert(ContentConstants.IMAGE_URI, values);
                    if (uri != null) {
                        iImageModel.setLocalId(Long.valueOf(uri.getLastPathSegment()));
                    }
                    return iImageModel;
                });
    }

    @Override
    public Observable.Transformer<IImageModel, IImageModel> saveTransformer() {
        return iImageModelObservable -> iImageModelObservable.flatMap(this::save);
    }

    @Override
    public Observable.Transformer<IImageModel, IImageModel> removeTransformer() {
        return iImageModelObservable -> iImageModelObservable.flatMap(this::remove);
    }

    @Override
    public Observable.Transformer<IImageModel, IImageModel> fetchLocalImageTransformer() {
        return iImageModelObservable -> iImageModelObservable.flatMap(image -> {
            return getByNetworkLink(image.getNetworkLink())
                    .map(imageDb -> {
                        if (imageDb != null) {
                            image.setLocalId(imageDb.getLocalId());
                            image.setLocalLink(imageDb.getLocalLink());
                        }
                        return image;
                    });
        });
    }

    private List<IImageModel> parseCursorToList(Cursor cursor) {
        List<IImageModel> images = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            int idIndex;
            int networkLinkIndex;
            int localLinkIndex;

            idIndex = cursor.getColumnIndex(ImageDatabaseHelper.KEY_ID);
            networkLinkIndex = cursor.getColumnIndex(ImageDatabaseHelper.KEY_NETWORK_LINK);
            localLinkIndex = cursor.getColumnIndex(ImageDatabaseHelper.KEY_LOCAL_LINK);
            do {
                IImageModel image = new ImageModel();
                image.setLocalId(cursor.getLong(idIndex));
                image.setNetworkLink(cursor.getString(networkLinkIndex));
                image.setLocalLink(cursor.getString(localLinkIndex));
                images.add(image);
            } while (cursor.moveToNext());
        }
        return images;
    }

    private IImageModel parseCursorToSingle(Cursor cursor) {
        IImageModel image = null;
        if (cursor != null && cursor.moveToFirst()) {
            int idIndex;
            int networkLinkIndex;
            int localLinkIndex;

            idIndex = cursor.getColumnIndex(ImageDatabaseHelper.KEY_ID);
            networkLinkIndex = cursor.getColumnIndex(ImageDatabaseHelper.KEY_NETWORK_LINK);
            localLinkIndex = cursor.getColumnIndex(ImageDatabaseHelper.KEY_LOCAL_LINK);

            image = new ImageModel();
            image.setLocalId(cursor.getLong(idIndex));
            image.setNetworkLink(cursor.getString(networkLinkIndex));
            image.setLocalLink(cursor.getString(localLinkIndex));
        }

        return image;
    }
}
