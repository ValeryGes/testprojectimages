package com.valeryges.testprojectimages.database;


import com.valeryges.testprojectimages.models.IImageModel;

import java.util.List;

import rx.Observable;

public interface IImageDaoModule {

    Observable<List<IImageModel>> getImagesLocal(int offset, int limit);

    Observable<IImageModel> getById(Long id);

    Observable<IImageModel> getByNetworkLink(String networkLink);

    Observable<IImageModel> remove(IImageModel iImageModel);

    Observable<IImageModel> save(IImageModel iImageModel);

    Observable.Transformer<IImageModel, IImageModel> saveTransformer();

    Observable.Transformer<IImageModel, IImageModel> removeTransformer();

    Observable.Transformer<IImageModel, IImageModel> fetchLocalImageTransformer();
}
