package com.valeryges.testprojectimages.models.converters;


import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Base implementation of {@link IConverter}
 *
 * @param <IN>  Input type
 * @param <OUT> Output type
 */
public abstract class BaseConverter<IN, OUT> implements IConverter<IN, OUT> {

    @Override
    public List<OUT> convertListInToOut(List<IN> inObjects) {
        if (inObjects == null) {
            return new ArrayList<>();
        }
        List<OUT> outList = new ArrayList<>();
        for (IN in : inObjects) {
            outList.add(convertINtoOUT(in));
        }
        return outList;
    }

    @Override
    public List<IN> convertListOutToIn(List<OUT> outObjects) {
        if (outObjects == null) {
            return  new ArrayList<>();
        }
        List<IN> inList = new ArrayList<>();
        for (OUT out : outObjects) {
            inList.add(convertOUTtoIN(out));
        }
        return inList;
    }

    @Override
    public Observable.Transformer<OUT, IN> singleINtoOUT() {
        return outObservable -> outObservable.map(BaseConverter.this::convertOUTtoIN);
    }

    @Override
    public Observable.Transformer<IN, OUT> singleOUTtoIN() {
        return inObservable -> inObservable.map(BaseConverter.this::convertINtoOUT);
    }

    @Override
    public Observable.Transformer<List<OUT>, List<IN>> listINtoOUT() {
        return outListObservable -> outListObservable.map(this::convertListOutToIn);
    }

    @Override
    public Observable.Transformer<List<IN>, List<OUT>> listOUTtoIN() {
        return inListObservable -> inListObservable.map(this::convertListInToOut);
    }
}
