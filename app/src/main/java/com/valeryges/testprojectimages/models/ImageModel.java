package com.valeryges.testprojectimages.models;


import android.os.Parcel;

public class ImageModel implements IImageModel {

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel source) {
            return new ImageModel(source);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };

    private Long localId;

    private String networkLink;

    private String localLink;

    public ImageModel() {
    }

    protected ImageModel(Parcel in) {
        this.localId = (Long) in.readValue(Long.class.getClassLoader());
        this.networkLink = in.readString();
        this.localLink = in.readString();
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getNetworkLink() {
        return networkLink;
    }

    @Override
    public void setNetworkLink(String networkLink) {
        this.networkLink = networkLink;
    }

    @Override
    public String getLocalLink() {
        return localLink;
    }

    @Override
    public void setLocalLink(String localLink) {
        this.localLink = localLink;
    }

    @Override
    public String getActualLink() {
        if (localLink != null) {
            return localLink;
        } else {
            return networkLink;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.localId);
        dest.writeString(this.networkLink);
        dest.writeString(this.localLink);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageModel)) {
            return false;
        }
        ImageModel that = (ImageModel) o;
        return getNetworkLink() != null
                ? getNetworkLink().equals(that.getNetworkLink())
                : that.getNetworkLink() == null;
    }

    @Override
    public int hashCode() {
        return getNetworkLink() != null ? getNetworkLink().hashCode() : 0;
    }
}
