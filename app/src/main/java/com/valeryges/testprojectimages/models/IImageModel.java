package com.valeryges.testprojectimages.models;


import android.os.Parcelable;

public interface IImageModel extends Parcelable {

    Long getLocalId();

    void setLocalId(Long localId);

    String getNetworkLink();

    void setNetworkLink(String networkLink);

    String getLocalLink();

    void setLocalLink(String localLink);

    String getActualLink();

}
