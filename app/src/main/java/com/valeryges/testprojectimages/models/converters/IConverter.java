package com.valeryges.testprojectimages.models.converters;


import android.support.annotation.Nullable;

import java.util.List;

import rx.Observable;

/**
 * Encapsulate logic for converting from one type to another and vice versa
 *
 * @param <IN>  Input type
 * @param <OUT> Output type
 */
public interface IConverter<IN, OUT> {

    /**
     * Convert IN to OUT
     *
     * @param inObject object ot convert
     * @return converted object
     */
    OUT convertINtoOUT(IN inObject);

    /**
     * Convert IN to OUT
     *
     * @param inObject object ot convert
     * @param payload  object with additional info for converting, can be ignored
     * @return converted object
     */
    OUT convertINtoOUT(IN inObject, @Nullable Object payload);

    /**
     * Convert OUT to IN
     *
     * @param outObject object ot convert
     * @return converted object
     */
    IN convertOUTtoIN(OUT outObject);

    /**
     * Convert OUT to IN
     *
     * @param outObject object ot convert
     * @param payload   object with additional info for converting, can be ignored
     * @return converted object
     */
    IN convertOUTtoIN(OUT outObject, @Nullable Object payload);

    /**
     * Convert List of IN to List of OUT
     *
     * @param inObjects {@link List} of objects to convert
     * @return {@link List} of converted objects
     */
    List<OUT> convertListInToOut(List<IN> inObjects);

    /**
     * Convert List of OUT to List of IN
     *
     * @param outObjects {@link List} of objects to convert
     * @return {@link List} of converted objects
     */
    List<IN> convertListOutToIn(List<OUT> outObjects);

    /**
     * Returns Transformer for converting Observable with single IN to OUT
     *
     * @return Instance of {@link rx.Observable.Transformer}
     */
    Observable.Transformer<OUT, IN> singleINtoOUT();

    /**
     * Returns Transformer for converting Observable with single OUT to IN
     *
     * @return Instance of {@link rx.Observable.Transformer}
     */
    Observable.Transformer<IN, OUT> singleOUTtoIN();

    /**
     * Returns Transformer for converting Observable with List of IN to List of OUT
     *
     * @return Instance of {@link rx.Observable.Transformer}
     */
    Observable.Transformer<List<OUT>, List<IN>> listINtoOUT();

    /**
     * Returns Transformer for converting Observable with List of OUT to List of IN
     *
     * @return Instance of {@link rx.Observable.Transformer}
     */
    Observable.Transformer<List<IN>, List<OUT>> listOUTtoIN();
}
