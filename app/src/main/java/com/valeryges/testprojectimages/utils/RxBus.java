package com.valeryges.testprojectimages.utils;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Util for sending events from different parts of application
 */
public final class RxBus {
    private final Subject<Object, Object> bus = new SerializedSubject<>(PublishSubject.create());

    /**
     * Returns current Instance of RxBus
     *
     * @return Instance of {@link RxBus}
     */
    public static RxBus instance() {
        return Loader.INSTANCE;
    }

    /**
     * Sends event with RxBus
     *
     * @param o Object representing some event
     */
    public void send(Object o) {
        bus.onNext(o);
    }

    private Observable<Object> toObservable() {
        return bus;
    }

    /**
     * Filtrates RxBus for events of some type
     *
     * @param eventClass Class of event to subscribe
     * @return Observable for event of some type
     */
    @SuppressWarnings("unchecked cast")
    public <T> Observable<T> filter(final Class<T> eventClass) {
        return toObservable()
                .filter(event -> event.getClass().equals(eventClass))
                .map(event -> (T) event);
    }

    private static final class Loader {
        private static final RxBus INSTANCE = new RxBus();
    }
}