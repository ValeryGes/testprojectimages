package com.valeryges.testprojectimages.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.valeryges.testprojectimages.TestProjectApp;


/**
 * Utils for helping with operations with network
 */
public final class NetworkUtils {
    private NetworkUtils() {
    }

    /**
     * Checks if device has established connection with Internet
     *
     * @return true if device has connection with Internet, false otherwise
     */
    public static boolean isConnected() {
        return isConnected(TestProjectApp.getInstance());
    }

    /**
     * Checks if device has established connection with Internet
     *
     * @param context instance of context
     * @return true if device has connection with Internet, false otherwise
     */
    @SuppressWarnings("deprecation")
    private static boolean isConnected(Context context) {
        if (context == null)
            return false;
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null)
            return false;
        final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return wifi != null && wifi.isConnected() || mobile != null && mobile.isConnected();
    }


}
