package com.valeryges.testprojectimages.utils;


import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.valeryges.testprojectimages.R;


/**
 * Util for showing custom snackBar {@link Snackbar}
 */
@SuppressWarnings("WeakerAccess")
public final class SnackBarUtil {

    private SnackBarUtil() {
        //no instance
    }

    /**
     * Shows Error SnackBar for long time
     *
     * @param view    SnackBar target
     * @param message Text for display
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showErrorLong(@NonNull View view, @NonNull String message) {
        return showError(view, message, Snackbar.LENGTH_LONG);
    }

    /**
     * Shows Error SnackBar for short time
     *
     * @param view    SnackBar target
     * @param message Text for display
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showErrorShort(@NonNull View view, @NonNull String message) {
        return showError(view, message, Snackbar.LENGTH_SHORT);
    }

    /**
     * Shows Error SnackBar
     *
     * @param view     SnackBar target
     * @param message  Text for display
     * @param duration displaying duration
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showError(@NonNull View view, @NonNull String message, int duration) {
        return showSnackBar(view, message, R.drawable.snackbar_alert, duration);
    }

    /**
     * Shows Success SnackBar for long time
     *
     * @param view    SnackBar target
     * @param message Text for display
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showSuccessLong(@NonNull View view, @NonNull String message) {
        return showSuccess(view, message, Snackbar.LENGTH_LONG);
    }

    /**
     * Shows Success SnackBar for short time
     *
     * @param view    SnackBar target
     * @param message Text for display
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showSuccessShort(@NonNull View view, @NonNull String message) {
        return showSuccess(view, message, Snackbar.LENGTH_SHORT);
    }

    /**
     * Shows Success SnackBar
     *
     * @param view     SnackBar target
     * @param message  Text for display
     * @param duration displaying duration
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showSuccess(@NonNull View view, @NonNull String message, int duration) {
        return showSnackBar(view, message, R.drawable.snackbar_ok, duration);
    }

    /**
     * Shows Warning SnackBar for long time
     *
     * @param view    SnackBar target
     * @param message Text for display
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showWarningLong(@NonNull View view, @NonNull String message) {
        return showWarning(view, message, Snackbar.LENGTH_LONG);
    }

    /**
     * Shows Warning SnackBar for short time
     *
     * @param view    SnackBar target
     * @param message Text for display
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showWarningShort(@NonNull View view, @NonNull String message) {
        return showWarning(view, message, Snackbar.LENGTH_SHORT);
    }

    /**
     * Shows Warning SnackBar
     *
     * @param view     SnackBar target
     * @param message  Text for display
     * @param duration displaying duration
     * @return Instance of {@link Snackbar}
     */
    public static Snackbar showWarning(@NonNull View view, @NonNull String message, int duration) {
        return showSnackBar(view, message, R.drawable.snackbar_warning, duration);
    }

    private static Snackbar showSnackBar(@NonNull View view, @NonNull String message, @DrawableRes int imageId, int duration) {
        Snackbar snackbar = Snackbar.make(view, "", duration);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        @SuppressLint("InflateParams")
        View snackView = LayoutInflater.from(view.getContext()).inflate(R.layout.snackbar_layout, null);
        TextView textViewTop = (TextView) snackView.findViewById(R.id.tvSnackBarText);
        ImageView imageViewSnack = (ImageView) snackView.findViewById(R.id.ivSnackBarIcon);
        imageViewSnack.setImageResource(imageId);
        textViewTop.setText(message);
        layout.addView(snackView, 0);
        snackbar.show();
        return snackbar;
    }

}
