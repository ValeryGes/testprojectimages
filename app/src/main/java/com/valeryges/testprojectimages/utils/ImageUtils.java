package com.valeryges.testprojectimages.utils;


import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Util for working with images
 */
public final class ImageUtils {

    private ImageUtils() {
    }

    /**
     * Loads image from uri to ImageView with placeHolder
     *
     * @param imageView   Instance of {@link ImageView} where you want set an Image
     * @param imageUri    Uri to the image
     * @param placeholder Drawable resource for placeholder
     */
    public static void loadImage(@NonNull ImageView imageView, @NonNull String imageUri, @DrawableRes int placeholder) {
        Glide.with(imageView.getContext())
                .load(imageUri)
                .centerCrop()
//                .placeholder(placeholder)
                .error(placeholder)
                .animate(android.R.anim.fade_in)
                .into(imageView);
    }

    public static void loadImageWithoutCrop(@NonNull ImageView imageView, @NonNull String imageUri) {
        Glide.with(imageView.getContext())
                .load(imageUri)
                .crossFade()
                .into(imageView);
    }
}
