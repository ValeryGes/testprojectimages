package com.valeryges.testprojectimages.utils;

import rx.Observable.Transformer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Util with helpful transformers
 */
public class Transformers {
    private Transformers() {
    }

    /**
     * Returns transformer witch makes {@link rx.Subscription} works in background thread
     * and call {@link rx.Observable#onSubscribe} in main thread.
     * See {@link rx.Observable#compose(Transformer)}
     *
     * @param <IN> Input and output type for {@link rx.Observable}
     * @return Instance of {@link Transformer}
     */
    public static <IN> Transformer<IN, IN> loadAsync() {
        return inObservable -> inObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
