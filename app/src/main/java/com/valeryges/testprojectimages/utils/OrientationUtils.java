package com.valeryges.testprojectimages.utils;


import android.app.Activity;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;

/**
 * Utils for checking current orientation of device
 */
public final class OrientationUtils {
    private OrientationUtils() {
    }

    /**
     * Checks if current orientation is portrait
     *
     * @param activity Instance of {@link Activity}
     * @return true if portrait, false in another case
     */
    public static boolean isPortrait(Activity activity) {
        return activity
                .getResources()
                .getConfiguration()
                .orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    /**
     * Checks if current orientation is landscape
     *
     * @param activity Instance of {@link Activity}
     * @return true if landscape, false in another case
     */
    public static boolean isLandscape(Activity activity) {
        return activity
                .getResources()
                .getConfiguration()
                .orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    /**
     * Checks if current orientation is portrait
     *
     * @param fragment Instance of {@link Fragment}
     * @return true if portrait, false in another case
     */
    public static boolean isPortrait(Fragment fragment) {
        return fragment
                .getResources()
                .getConfiguration()
                .orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    /**
     * Checks if current orientation is landscape
     *
     * @param fragment Instance of {@link Fragment}
     * @return true if landscape, false in another case
     */
    public static boolean isLandscape(Fragment fragment) {
        return fragment
                .getResources()
                .getConfiguration()
                .orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

}
