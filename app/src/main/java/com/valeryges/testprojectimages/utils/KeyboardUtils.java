package com.valeryges.testprojectimages.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Keyboard utils
 */
public class KeyboardUtils {

    /**
     * Hide keyboard
     *
     * @param activity Activity
     */
    public static void hideKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        View currentFocusView = activity.getCurrentFocus();
        if (currentFocusView != null) {
            InputMethodManager iim = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            iim.hideSoftInputFromWindow(currentFocusView.getWindowToken(), 0);

        }
    }

    /**
     * Hide keyboard
     *
     * @param fragment Fragment
     */
    public static void hideKeyboard(Fragment fragment) {
        hideKeyboard(fragment.getActivity());
    }
}
