package com.valeryges.testprojectimages.utils;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Util for working with Subscriptions {@link Subscription}
 */
public final class RxUtils {

    private RxUtils() {
        // no instance
    }

    /**
     * Checks Subscription for null and unsubscribe it in positive case.
     *
     * @param subscription Instance of {@link Subscription}
     */
    public static void unsubscribeIfNotNull(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    /**
     * checks existed CompositeSubscription for null and is it unsubscribed.
     * If check passed positively - returns this subscription. Or new one in another case
     *
     * @param subscription Instance of {@link CompositeSubscription}
     * @return Instance of {@link CompositeSubscription}
     */
    public static CompositeSubscription getNewCompositeSubIfUnsubscribed(CompositeSubscription subscription) {
        if (subscription == null || subscription.isUnsubscribed()) {
            return new CompositeSubscription();
        }

        return subscription;
    }
}
