package com.valeryges.testprojectimages.mvp;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Base implementation of {@link IView} for {@link AppCompatActivity}
 * Extends from {@link BaseLoaderActivity}
 *
 * @param <TPresenter>
 */
public abstract class BaseLoaderActivityView<TPresenter extends IPresenter> extends BaseLoaderActivity
        implements IView<TPresenter> {

    @Nullable
    private TPresenter presenter;

    @Override
    public void attachPresenter(@Nullable TPresenter presenter) {
        this.presenter = presenter;
    }

    @Nullable
    @Override
    public TPresenter getPresenter() {
        return presenter;
    }
}
