package com.valeryges.testprojectimages.mvp;

import android.content.Context;
import android.support.v4.app.LoaderManager;

/**
 * Encapsulate logic for creating loaders
 */
@SuppressWarnings("WeakerAccess")
interface ILoaderCreator {
    /**
     * Returns context for creating loader callbacks
     *
     * @return Instance of {@link Context}
     */
    Context getContextForCreator();

    /**
     * Returns LoaderManager for working with loaders
     *
     * @return Instance of {@link LoaderManager}
     */
    LoaderManager getLoaderManagerForCreator();
}
