package com.valeryges.testprojectimages.mvp;

import android.support.annotation.Nullable;

/**
 * Interface for View
 * @param <TPresenter> Type of presenter
 */
public interface IView<TPresenter extends IPresenter> {

    /**
     * Attaches Presenter to View. For detaching pass null in arguments
     *
     * @param presenter Instance of {@link TPresenter}
     */
    void attachPresenter(@Nullable TPresenter presenter);

    /**
     * Returns Presenter if it attached or null in another case
     *
     * @return Instance of {@link TPresenter} or null
     */
    @Nullable
    TPresenter getPresenter();
}
