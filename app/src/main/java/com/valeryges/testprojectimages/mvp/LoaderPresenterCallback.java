package com.valeryges.testprojectimages.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.valeryges.testprojectimages.funcs.Func2;


/**
 * Callback for PresenterLoader {@link PresenterLoader}
 *
 * @param <TPresenter> Type of Presenter
 * @param <TView>      Type of View
 */
class LoaderPresenterCallback<TPresenter extends IPresenter<TView>, TView extends IView<TPresenter>>
        implements LoaderManager.LoaderCallbacks<TPresenter>, IViewLifecycle {


    private final Context context;
    private final Func2<TPresenter> presenterCreator;
    private final TView view;
    private TPresenter presenter;
    private boolean isResumeCalled;


    private LoaderPresenterCallback(@NonNull Context context,
                                    @NonNull TView view,
                                    @NonNull Func2<TPresenter> presenterCreator) {
        this.context = context.getApplicationContext();
        this.view = view;
        this.presenterCreator = presenterCreator;
    }

    static <TPresenter extends IPresenter<TView>, TView extends IView<TPresenter>> LoaderPresenterCallback<TPresenter, TView>
    create(@NonNull Context context, @NonNull TView view, @NonNull Func2<TPresenter> presenterCreator) {
        return new LoaderPresenterCallback<>(context, view, presenterCreator);
    }

    @Override
    public void onResume() {
        isResumeCalled = true;
        checkAndCallResume();
    }

    @Override
    public void onPause() {
        isResumeCalled = false;
        if (presenter != null) {
            final TView view = presenter.getView();
            if (view == this.view) {
                this.view.attachPresenter(null);
            }
            presenter.attachView(null);
            presenter.onPause();
        }
    }

    private void checkAndCallResume() {
        if (presenter != null) {
            view.attachPresenter(presenter);
            presenter.attachView(view);
            presenter.onResume();
        }
    }

    @Override
    public Loader<TPresenter> onCreateLoader(int id, Bundle args) {
        return new PresenterLoader<>(context, presenterCreator);
    }

    @Override
    public void onLoadFinished(Loader<TPresenter> loader, TPresenter data) {
        presenter = data;
        if (isResumeCalled) {
            checkAndCallResume();
        }
    }

    @Override
    public void onLoaderReset(Loader<TPresenter> loader) {
        if (presenter != null) {
            presenter.attachView(null);
        }
        presenter = null;
    }

}
