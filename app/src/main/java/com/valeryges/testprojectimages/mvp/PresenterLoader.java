package com.valeryges.testprojectimages.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;

import com.valeryges.testprojectimages.funcs.Func2;


/**
 * Loader for loading presenter
 *
 * @param <TPresenter> Type of Presenter
 */
final class PresenterLoader<TPresenter extends IPresenter> extends Loader<TPresenter> {

    private TPresenter presenter;
    private final Func2<TPresenter> presenterCreator;

    /**
     * @param context          Instance of Context {@link Context}
     * @param presenterCreator functional interface that returns Instance of {@link TPresenter}
     */
    PresenterLoader(@NonNull Context context, @NonNull Func2<TPresenter> presenterCreator) {
        super(context);
        this.presenterCreator = presenterCreator;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (takeContentChanged() || presenter == null) {
            forceLoad();
            return;
        }
        deliverResult(presenter);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();

    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        cancelLoad();
        presenter = presenterCreator.call();
        presenter.onPresenterCreated();
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (presenter != null) {
            presenter.onPresenterDestroyed();
            presenter = null;
        }
    }
}
