package com.valeryges.testprojectimages.mvp;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * Base implementation of {@link IView} for {@link Fragment}
 * Extends from {@link BaseLoaderFragment}
 *
 * @param <TPresenter>
 */
public abstract class BaseLoaderFragmentView<TPresenter extends IPresenter> extends BaseLoaderFragment implements
        IView<TPresenter> {

    @Nullable
    private TPresenter presenter;

    @Override
    public void attachPresenter(@Nullable TPresenter presenter) {
        this.presenter = presenter;
    }

    @Nullable
    @Override
    public TPresenter getPresenter() {
        return presenter;
    }
}
