package com.valeryges.testprojectimages.mvp;

import android.support.annotation.Nullable;

/**
 * Base implementation of {@link IPresenter}
 *
 * @param <TView> Type of View
 */
public class BasePresenter<TView extends IView> implements IPresenter<TView> {

    @Nullable
    private TView view;

    @Nullable
    @Override
    public TView getView() {
        return view;
    }

    @Override
    public void attachView(@Nullable TView view) {
        this.view = view;
    }

    @Override
    public void onResume() {
        // TODO: override method if you need it
    }

    @Override
    public void onPause() {
        // TODO: override method if you need it
    }

    @Override
    public void onPresenterCreated() {
        // TODO: override method if you need it
    }

    @Override
    public void onPresenterDestroyed() {
        // TODO: override method if you need it
    }
}
