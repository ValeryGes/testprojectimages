package com.valeryges.testprojectimages.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;

import com.valeryges.testprojectimages.funcs.Func2;


/**
 * Base implementation of {@link ILoaderInitializer} and {@link ILoaderCreator}
 * for {@link AppCompatActivity}
 */
public abstract class BaseLoaderActivity extends AppCompatActivity implements ILoaderInitializer, ILoaderCreator {

    private final ILoaderInitializer loaderInitializer;

    protected BaseLoaderActivity() {
        loaderInitializer = new LoaderInitializer(this);
    }

    @Override
    public <TPresenter extends IPresenter<TView>, TView extends IView<TPresenter>>
    void initPresenter(int loaderId, @NonNull TView view, @NonNull Func2<TPresenter> presenterCreator) {
        loaderInitializer.initPresenter(loaderId, view, presenterCreator);
    }

    @Override
    public void onResume() {
        super.onResume();
        loaderInitializer.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        loaderInitializer.onPause();
    }

    @Override
    public LoaderManager getLoaderManagerForCreator() {
        return getSupportLoaderManager();
    }

    @Override
    public Context getContextForCreator() {
        return this;
    }
}
