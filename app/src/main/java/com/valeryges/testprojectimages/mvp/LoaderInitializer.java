package com.valeryges.testprojectimages.mvp;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.valeryges.testprojectimages.funcs.Func2;


/**
 * Delegate class encapsulates all logic for creation Loader and handling callbacks in OnResume
 * and OnPause
 */
final class LoaderInitializer implements ILoaderInitializer {

    private final SparseArray<LoaderPresenterCallback<?, ?>> loaderCallbacks = new SparseArray<>();
    private final ILoaderCreator loaderCreator;

    public LoaderInitializer(ILoaderCreator loaderCreator) {
        this.loaderCreator = loaderCreator;
    }

    @Override
    public <TPresenter extends IPresenter<TView>, TView extends IView<TPresenter>>
    void initPresenter(int loaderId, @NonNull TView view, @NonNull Func2<TPresenter> presenterCreator) {
        final LoaderPresenterCallback<TPresenter, TView> loaderCallback;
        loaderCallback = LoaderPresenterCallback.create(loaderCreator.getContextForCreator(), view, presenterCreator);
        loaderCreator.getLoaderManagerForCreator().initLoader(loaderId, null, loaderCallback);
        this.loaderCallbacks.put(loaderId, loaderCallback);
    }


    @Override
    public void onResume() {
        final int size = loaderCallbacks.size();
        for (int i = 0; i < size; i++) {
            final LoaderPresenterCallback<?, ?> presenterLoaderHandler = loaderCallbacks.valueAt(i);
            if (loaderCreator != null) {
                presenterLoaderHandler.onResume();
            }
        }
    }

    @Override
    public void onPause() {
        final int size = loaderCallbacks.size();
        for (int i = 0; i < size; i++) {
            final LoaderPresenterCallback<?, ?> presenterLoaderHandler = loaderCallbacks.valueAt(i);
            presenterLoaderHandler.onPause();
        }
    }

}
