package com.valeryges.testprojectimages.mvp;

import android.support.annotation.NonNull;

import com.valeryges.testprojectimages.funcs.Func2;


/**
 * Encapsulate logic for initialization of presenter
 */
@SuppressWarnings("WeakerAccess")
public interface ILoaderInitializer extends IViewLifecycle {

    /**
     * Creates presenter and connect it to the view
     *
     * @param loaderId         id for loader
     * @param view             Instance of View
     * @param presenterCreator Functional interface witch returns the Instance of Presenter
     * @param <TPresenter>     Type of Presenter
     * @param <TView>          Type of View
     */
    <TPresenter extends IPresenter<TView>, TView extends IView<TPresenter>>
    void initPresenter(int loaderId, @NonNull TView view, @NonNull Func2<TPresenter> presenterCreator);
}
