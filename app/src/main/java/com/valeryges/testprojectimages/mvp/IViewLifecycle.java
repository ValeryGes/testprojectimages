package com.valeryges.testprojectimages.mvp;

/**
 * Represents LifeCycle of View
 */
interface IViewLifecycle {

    /**
     * Calls when view calls OnResume
     */
    void onResume();

    /**
     * Calls when view calls OnPause
     */
    void onPause();

}
