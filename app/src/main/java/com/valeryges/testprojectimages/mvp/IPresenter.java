package com.valeryges.testprojectimages.mvp;

import android.support.annotation.Nullable;

/**
 * Interface for Presenter
 *
 * @param <TView> Type of View
 */
@SuppressWarnings("WeakerAccess")
public interface IPresenter<TView extends IView> extends IViewLifecycle {

    /**
     * Returns View if it attached or null in another case
     *
     * @return Instance of {@link TView} or null
     */
    @Nullable
    TView getView();

    /**
     * Attaches View to Presenter. For detaching pass null in arguments
     *
     * @param view Instance of {@link TView}
     */
    void attachView(@Nullable TView view);

    /**
     * Calls when presenter created
     */
    void onPresenterCreated();

    /**
     * Calls when presenter destroyed
     */
    void onPresenterDestroyed();
}
