package com.valeryges.testprojectimages.funcs;

/**
 * Functional interface
 *
 * @param <T> Type passing in argument
 * @param <R> Type passing in return
 */
@FunctionalInterface
public interface Func3<T, R> {
    R call(T object);
}
