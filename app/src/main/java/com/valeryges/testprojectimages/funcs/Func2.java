package com.valeryges.testprojectimages.funcs;

/**
 * Functional interface
 *
 * @param <R> Type passing in return
 */
@FunctionalInterface
public interface Func2<R> {
    R call();
}
