package com.valeryges.testprojectimages.funcs;

/**
 * Functional interface
 *
 * @param <T> Type passing in argument
 */
@FunctionalInterface
public interface Func1<T> {
    void call(T object);
}
